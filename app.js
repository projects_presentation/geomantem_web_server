var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require("./session");

var login = require('./routes/login');
var brands = require('./routes/brands');
var models = require('./routes/models');
var equipments = require('./routes/equipments');
var clients = require('./routes/clients');
var clients_accounts = require('./routes/clients_accounts');
var technicians_accounts = require('./routes/technicians_accounts');
var repairs = require('./routes/repairs');
var modelsRepairs = require('./routes/models_repairs');
var statuses = require('./routes/statuses');
var client = require('./routes/client');
var routes = require('./routes/routes');
var route_points = require('./routes/route_points');
var reports = require('./routes/reports');
var logs = require('./routes/logs');
var requests = require('./routes/requests');
var client_requests = require('./routes/client_requests');
var equipment_repair = require('./routes/equipment_repair');
var photos = require('./routes/photos');
var warehouses = require('./routes/warehouses');
var email = require('./routes/email');

/* HTTPS */
var fs = require('fs');
var https = require('https');
var privateKey = fs.readFileSync('./ssl/key.pem', 'utf8');
var certificate = fs.readFileSync('./ssl/cert.pem', 'utf8');
var credentials = { key: privateKey, cert: certificate };
/**/

var app = express();
app.enable('trust proxy');
app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true,parameterLimit:50000}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

session.setupSession(app);

app.use(function(req, res, next) {
    if (req.secure) {
        // request was via https, so do no special handling
        next();
    } else {
        // request was via http, so redirect to https
        res.redirect('https://' + req.headers.host + req.url);
    }
});

app.use('/', login, express.static(path.join(__dirname, 'public')));
app.use('/brands', brands, express.static(path.join(__dirname, 'public')));
app.use('/models', models, express.static(path.join(__dirname, 'public')));
app.use('/equipments', equipments, express.static(path.join(__dirname, 'public')));
app.use('/clients', clients, express.static(path.join(__dirname, 'public')));
app.use('/clients_accounts', clients_accounts, express.static(path.join(__dirname, 'public')));
app.use('/technicians_accounts', technicians_accounts, express.static(path.join(__dirname, 'public')));
app.use('/accounts/settings', clients_accounts, express.static(path.join(__dirname, 'public')));
app.use('/accounts/settings/settings', clients_accounts, express.static(path.join(__dirname, 'public')));
app.use('/repairs', repairs, express.static(path.join(__dirname, 'public')));
app.use('/models_repairs', modelsRepairs);
app.use('/statuses', statuses);
app.use('/client', client, express.static(path.join(__dirname, 'public')));
app.use('/routes', routes, express.static(path.join(__dirname, 'public')));
app.use('/route_points', route_points, express.static(path.join(__dirname, 'public')));
app.use('/reports', reports, express.static(path.join(__dirname, 'public')));
app.use('/equipment_repair', equipment_repair, express.static(path.join(__dirname, 'public')));
app.use('/logs', logs, express.static(path.join(__dirname, 'public')));
app.use('/requests', requests, express.static(path.join(__dirname, 'public')));
app.use('/client_requests', client_requests, express.static(path.join(__dirname, 'public')));
app.use('/photos', photos, express.static(path.join(__dirname, 'public')));
app.use('/warehouses', warehouses, express.static(path.join(__dirname, 'public')));
app.use('/email', email);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

var server_port = process.env.NODE_ENV === 'development' ? 8443 : 443;

var httpsServer = https.createServer(credentials, app);
httpsServer.listen(server_port);

//app.listen(process.env.port || server_port);

console.log('Server is running at port: ' + server_port);


module.exports = app;