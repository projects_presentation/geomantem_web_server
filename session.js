var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var expressValidator = require('express-validator');

var mysql = require('./mysql/connection_manager');
var MySQLStoreSession;

function setupSession(app) {

    app.use(expressValidator());
    app.use(cookieParser());

    MySQLStoreSession = require('express-mysql-session')(expressSession);
    var sessionStore = new MySQLStoreSession({ createDatabaseTable: true }, mysql.getPool());
    app.use(expressSession({
        secret: 'p@22w0rd',
        resave: false,
        saveUninitialized: false,
        store: sessionStore,
        cookie: { secure: false }
    }));
}

module.exports.expressSession = expressSession;
module.exports.setupSession = setupSession;