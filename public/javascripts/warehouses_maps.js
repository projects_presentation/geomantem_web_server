function updateMap(data) {
    var map = new google.maps.Map(document.getElementById("map"));

    var styles = {
        hide: [{
                featureType: 'poi.business',
                stylers: [{ visibility: 'off' }]
            },
            {
                featureType: 'transit',
                elementType: 'labels.icon',
                stylers: [{ visibility: 'off' }]
            }
        ]
    };

    map.addListener('dblclick', function(event) {
        createWarehouse(event.latLng.lat(), event.latLng.lng());
    });

    map.setOptions({ disableDoubleClickZoom: true, styles: styles['hide'] });

    if (data.length === 0) {
        centerOnCountry(map);
    } else {
        var bounds = new google.maps.LatLngBounds();
        $.each(data, function(index, value) {
            marker = GetMarker(value, map);
            setMarkerListener(marker, value, map);
            bounds.extend(new google.maps.LatLng(value.latitude, value.longitude));
        });
        map.fitBounds(bounds);

        var myMarker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP
        });
    }

    setCustomZoomButton(map);
}

function setCustomZoomButton(map) {

    // Create the DIV to hold the control and call the ZoomControl() constructor
    // passing in this DIV.
    var zoomControlDiv = document.createElement('div');

    // Creating divs & styles for custom zoom control
    zoomControlDiv.style.padding = '5px';

    // Set CSS for the control wrapper
    var controlWrapper = document.createElement('div');
    controlWrapper.style.backgroundColor = 'white';
    controlWrapper.style.borderStyle = 'solid';
    controlWrapper.style.borderColor = 'gray';
    controlWrapper.style.borderWidth = '1px';
    controlWrapper.style.cursor = 'pointer';
    controlWrapper.style.textAlign = 'center';
    controlWrapper.style.width = '150px';
    controlWrapper.style.height = '75px';
    controlWrapper.style.display = 'inline-flex';
    zoomControlDiv.appendChild(controlWrapper);

    // Set CSS for the zoomIn
    var zoomInButton = document.createElement('div');
    zoomInButton.style.width = '75px';
    zoomInButton.style.height = '75px';
    /* Change this to be the .png image you want to use */
    zoomInButton.style.backgroundImage = 'url("/images/zoom_in.png")';
    controlWrapper.appendChild(zoomInButton);

    // Set CSS for the zoomOut
    var zoomOutButton = document.createElement('div');
    zoomOutButton.style.width = '75px';
    zoomOutButton.style.height = '75px';
    /* Change this to be the .png image you want to use */
    zoomOutButton.style.backgroundImage = 'url("/images/zoom_out.png")';
    controlWrapper.appendChild(zoomOutButton);

    // Setup the click event listener - zoomIn
    google.maps.event.addDomListener(zoomInButton, 'click', function() {
        map.setZoom(map.getZoom() + 1);
    });

    // Setup the click event listener - zoomOut
    google.maps.event.addDomListener(zoomOutButton, 'click', function() {
        map.setZoom(map.getZoom() - 1);
    });

    zoomControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(zoomControlDiv);
}

function setMarkerListener(marker, warehouse, map) {

    google.maps.event.addListener(marker, 'dragend', function(event) {
        warehouse.latitude = this.getPosition().lat();
        warehouse.longitude = this.getPosition().lng();
        $.ajax({
            type: "POST",
            url: "/warehouses/update",
            data: warehouse
        });
    });
}

function centerOnCountry(map) {
    var country = "Portugal";
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': country }, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            var reference = new google.maps.LatLng(latitude, longitude);
            map.setZoom(7);
            map.setCenter(reference);
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP);

        } else {
            alert("Could not find location: " + location);
        }
    });
}

function GetMarker(equipment, map) {
    var image = {};
    image.scaledSize = new google.maps.Size(25, 25);
    image.url = '/images/warehouse.png';
    var markerToReturn = new MarkerWithLabel({
        icon: image,
        position: new google.maps.LatLng(equipment.latitude, equipment.longitude),
        draggable: dragEquipments,
        map: map,
        title: ' Descrição: ' + equipment.name,
        labelContent: equipment.serial_number,
        labelAnchor: new google.maps.Point(25, 50),
        labelClass: 'equipment_label' + equipment.status_id,
        labelInBackground: true
    });
    return markerToReturn;
}