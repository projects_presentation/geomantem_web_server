var clientAccount

function setClientAccount(account) {
    clientAccount = account;
}

function fillRequestsTable(autorize_repairs) {

    jsGrid.fields.myDateField = getDateField();

    $("#requestsGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: false,
        editing: false,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,

        deleteConfirm: "Deseja realmente apagar o pedido de intrevenção?",

        rowClass: function(item, itemIndex) {
            if (item.status_id === 5) {
                return 'selected-row-orange';
            } else {
                return item.ended === null ? "selected-row-red" : "selected-row-green";
            }
        },

        controller: {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "/client_requests/get",
                    data: filter
                });
            },
            deleteItem: function(item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/client_requests/delete",
                    data: item
                });
            }
        },

        fields: getTableFields(autorize_repairs)
    });
};

function getTableFields(autorize_repairs) {
    var fields = [{
            name: "id",
            type: "text",
            title: "Número de pedido",
            align: "center",
        },
        {
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center",
        },
        {
            name: "text",
            type: "text",
            title: "Pedido",
            align: "center",
        },
        {
            title: 'Imagem',
            name: "photo_url",
            itemTemplate: function(val, item) {
                return $("<img>").attr("src", '/equipments/images/' + val).css({ height: 50, width: 50 }).on("click", function() {
                    $("#imagePreview").attr("src", item.photo_url);
                    $("#dialog").dialog("open");
                });
            },
            align: "center",
            width: 120
        },
        { name: "started", type: "myDateField", title: "Data do pedido", width: 150, align: "center", readOnly: true },
        {
            name: "response",
            type: "text",
            title: "Resposta recebida",
            align: "center"
        },
        { name: "ended", type: "myDateField", title: "Data de fecho do pedido", width: 150, align: "center", readOnly: true },
        {
            width: 100,
            itemTemplate: function(_, item) {
                if (autorize_repairs === 1 && item.status_id === 5) {
                    return $('<button type="button" class="btn btn-success">Autorizar Reparação</button>')
                        .on("click", function() {
                            $.ajax({
                                type: "POST",
                                url: '/equipments/update_status/',
                                data: {
                                    equipmentID: item.equipment_id,
                                    newStatus: 2
                                },
                                success: function(data) {
                                    if (data === true) {
                                        showOkResponsePopup('Autorizar reparação de equipamento', 'O pedido de autorização foi enviado com sucesso');
                                    } else {
                                        showErrorPopup('Autorizar reparação de equipamento', 'O pedido de autorização não foi enviado com sucesso, por favor tente mais tarde...');
                                    }
                                    $("#requestsGrid").jsGrid("loadData");
                                },
                            });
                        });
                }
            }
        },
        { type: "control", editButton: false, deleteButton: false }
    ];

    return fields;
}

function getDateField() {
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function(value) {
            var date = moment(value);

            var dateToReturn = date.format("YYYY-MM-DD HH:mm:ss");
            if (dateToReturn.indexOf("Invalid") !== -1) {
                dateToReturn = 'Ainda não obteve resposta';
            }
            return dateToReturn;
        },
    });
    return MyDateField;
}

function sendRequest() {

    var selectHTML = '<select id="selectedEquipment" class="selectpicker">';

    $.ajax({
        type: "GET",
        url: '/client_requests/get_equipments/',
        success: function(data) {
            data.forEach(function(element) {
                selectHTML += '<option value=' + element.id + '>' + element.serial_number + '</option>';
            }, this);
            selectHTML += '</select>'
            showRequestBox(selectHTML);
        },
    });
}

function showRequestBox(selectHTML) {
    $.ajax({
        type: 'GET',
        url: '/requests/add_photo_to_request',
        success: function(addPhotoHTML) {
            BootstrapDialog.show({
                title: 'Enviar pedido de intervenção',
                message: $(selectHTML + '<p>' + addPhotoHTML + '<p><textarea placeholder="Introduza o pedido..." id="requestText" style="width: 100%;height: 100px; margin-top: 30px;"></textarea><script>$(".selectpicker").selectpicker();</script>'),
                buttons: [{
                    label: 'Enviar pedido',
                    cssClass: 'btn-primary',
                    hotkey: 13,
                    action: function(dialogRef) {
                        var requestText = document.getElementById('requestText').value;
                        var selectedEquipment = document.getElementById('selectedEquipment').value;
                        var serialNumber = document.getElementById('selectedEquipment').selectedOptions[0].innerHTML;

                        if (requestText.length > 500) {
                            showErrorPopup('Enviar pedido de intervenção', 'O pedido de intervenção não deve conter mais do que 500 caracteres');
                            $("#name").val("");
                            return;
                        }

                        dialogRef.close();
                        $.ajax({
                            type: "POST",
                            url: '/client_requests/insert/',
                            data: {
                                equipment_id: selectedEquipment,
                                text: requestText,
                                photo_url: uploadedFilename === undefined ? 'no_image.png' : uploadedFilename
                            },
                            success: function(data) {
                                if (data === true) {
                                    showOkResponsePopup('Enviar pedido de intervenção', 'O pedido de intervenção foi enviado com sucesso');
                                    $.ajax({
                                        type: 'POST',
                                        url: '/email/send_email/',
                                        data: {
                                            to: 'admin@geomantem.pt',
                                            subject: 'Novo pedido de intervenção',
                                            text: 'Cliente: ' + clientAccount.username + '\n\nNúmero de série: ' + serialNumber + '\n\nTexto do pedido: ' + requestText
                                        }
                                    });
                                } else {
                                    showErrorPopup('Enviar pedido de intervenção', 'O pedido de intervenção não foi enviado com sucesso, por favor tente mais tarde...');
                                }

                            },
                        });

                    }
                }, {
                    label: 'Cancelar',
                    action: function(dialogRef) {
                        dialogRef.close();
                    }
                }]
            });
        }
    })
}

function showOkResponsePopup(title, messageToShow) {
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: title,
        message: messageToShow,
        buttons: [{
            label: 'Ok',
            action: function(dialogRef) {
                dialogRef.close();
                window.location.href = '/client_requests/';
            }
        }]
    });
}