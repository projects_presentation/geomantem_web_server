function fillModelsTable() {

    $.ajax({
        type: "GET",
        url: "/brands/get",
    }).done(function(brands) {

        brands.unshift({ id: "0", name: "" });

        $("#modelsGrid").jsGrid({
            width: "90%",
            filtering: true,
            inserting: true,
            editing: true,
            sorting: true,
            paging: true,
            autoload: true,
            pageSize: 10,
            pageButtonCount: 5,

            deleteConfirm: "Deseja realmente apagar a Nome / Marca / Modelo?",
            invalidMessage: "Dados de preenchimento são inválidos",

            rowClick: function(args) {
                fillModelsRepairsTable(args.item.id);
            },


            controller: {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        url: "/models/get",
                        data: filter
                    });
                },
                insertItem: function(item) {
                    return $.ajax({
                        type: "POST",
                        url: "/models/insert",
                        data: item
                    });
                },
                updateItem: function(item) {
                    return $.ajax({
                        type: "POST",
                        url: "/models/update",
                        data: item
                    });
                },
                deleteItem: function(item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/models/delete",
                        data: item
                    });
                }
            },

            fields: getTableFields(brands)
        });
    });


};

function getTableFields(brands) {
    var fields = [{
            name: "name",
            type: "text",
            title: "Nome",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O nome da Nome / Marca / Modelo deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        {
            name: "brand_id",
            type: "select",
            title: "Tipo de equipamento / Serviço",
            items: brands,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo Tipo de equipamento / Serviço não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.brand_id !== 0;
                },
            },
        },
        {
            name: "icon",
            type: "text",
            title: "Icon no mapa",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O icon no mapa deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        { type: "control" }
    ];

    return fields;
}