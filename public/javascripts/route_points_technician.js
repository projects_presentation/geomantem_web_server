function fillRoutePointsTable(route) {

    $.ajax({
        type: "GET",
        url: "/statuses/get"
    }).done(function(statuses) {
        $.ajax({
            type: "GET",
            url: "/clients/get"
        }).done(function(clients) {
            jsGrid.fields.myDateFieldStart = getDateField(true);
            jsGrid.fields.myDateFieldEnd = getDateField(false);

            statuses.unshift({ id: "0", name: "" });
            clients.unshift({ id: "0", name: "" });

            $("#routePointsGrid").jsGrid({
                width: "90%",
                filtering: false,
                inserting: false,
                editing: false,
                sorting: true,
                paging: true,
                autoload: true,
                pageSize: 10,
                pageButtonCount: 5,

                rowClass: function(item, itemIndex) {
                    return item.ended === null ? "selected-row-red" : "selected-row-green";
                },

                rowClick: function(args) {
                    if (route.started === null) {
                        showErrorPopup('Iniciar reparação', 'Por favor inicie primeiro a rota, antes de reparar o equipamento\nSe já iniciou a rota actualize a página');
                    } else {
                        startRoutePointRepairPopup(args.item);
                    }

                },

                onRefreshed: function(args) {
                    document.getElementById('route_points_report').style.display = 'block';
                    updateMap(args.grid.data);
                },

                controller: {
                    loadData: function(filter) {
                        filter.route_id = route.id;
                        return $.ajax({
                            type: "GET",
                            url: "/route_points/get_formated",
                            data: filter
                        });
                    }
                },

                fields: getTableFieldsForRoutePoints(statuses, clients)
            });
        });
    });
};

function getTableFieldsForRoutePoints(statuses, clients) {
    var fields = [{
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center"
        },
        {
            name: "characteristics",
            type: "text",
            title: "Características",
            align: "center"
        },
        {
            name: "brand_name",
            type: "text",
            title: "Tipo de equipamento / Serviço",
            align: "center",
        },
        {
            name: "model_name",
            type: "text",
            title: "Nome / Marca / Modelo",
            align: "center",
        },
        {
            name: "client_id",
            type: "select",
            title: "Cliente",
            items: clients,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0
        },
        {
            name: "status_id",
            type: "select",
            title: "Estado",
            items: statuses,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0
        },
        {
            name: "warehouse_name",
            type: "text",
            title: "Estaleiro",
            align: "center"
        },
        { name: "started", type: "myDateFieldStart", title: "Iniciou reparação", align: "center", readOnly: true },
        { name: "ended", type: "myDateFieldEnd", title: "Terminou reparação", align: "center", readOnly: true }
    ];

    return fields;
}

function getDateField(start) {
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function(value) {
            var date = moment(value);

            var dateToReturn = date.format("YYYY-MM-DD HH:mm:ss");
            if (dateToReturn.indexOf("Invalid") !== -1) {
                dateToReturn = (start === true ? "Não iniciou reparação" : " Não terminou reparação");
            }
            return dateToReturn;
        },
    });
    return MyDateField;
}