var dragEquipments = false;

function fillWarehousesTable() {

    $('#dragEquipmentsCheckBox').change(function() {
        dragEquipments = $(this).prop('checked');
        updateMap($("#warehousesGrid").jsGrid("option", "data"));
    });

    $.ajax({
        type: "GET",
        url: "/clients/get",
    }).done(function(clients) {

        clients.unshift({ id: "0", name: "" });

        $("#warehousesGrid").jsGrid({
            width: "90%",
            filtering: true,
            inserting: true,
            editing: true,
            sorting: true,
            paging: true,
            autoload: true,
            pageSize: 10,
            pageButtonCount: 5,

            deleteConfirm: "Deseja realmente apagar o estaleiro?",
            invalidMessage: "Dados de preenchimento são inválidos",

            onRefreshed: function(args) {
                updateMap(args.grid.data);
            },

            controller: {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        url: "/warehouses/get",
                        data: filter
                    });
                },
                insertItem: function(item) {
                    item.latitude = 0;
                    item.longitude = 0;
                    return $.ajax({
                        type: "POST",
                        url: "/warehouses/insert",
                        data: item
                    });
                },
                updateItem: function(item) {
                    return $.ajax({
                        type: "POST",
                        url: "/warehouses/update",
                        data: item
                    });
                },
                deleteItem: function(item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/warehouses/delete",
                        data: item
                    });
                }
            },

            fields: getTableFields(clients)
        });
    });
}

function getTableFields(clients) {
    var fields = [{
            name: "name",
            type: "text",
            title: "Nome",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O nome do estaleiro deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        {
            name: "client_id",
            type: "select",
            title: "Cliente",
            items: clients,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo cliente não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.client_id !== 0;
                },
            },
        },
        { type: "control" }
    ];

    return fields;
}