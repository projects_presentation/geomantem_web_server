function fillBrandsTable() {

    jsGrid.fields.myDateField = GetDateField();

    $("#brandsGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,

        deleteConfirm: "Deseja realmente apagar o Tipo de equipamento / Serviço?",
        invalidMessage: "Dados de preenchimento são inválidos",

        controller: {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "/brands/get",
                    data: filter
                });
            },
            insertItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "/brands/insert",
                    data: item
                });
            },
            updateItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "/brands/update",
                    data: item
                });
            },
            deleteItem: function(item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/brands/delete",
                    data: item
                });
            }
        },

        fields: getTableFields()
    });
};

function getTableFields() {
    var fields = [{
            name: "name",
            type: "text",
            title: "Nome",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O Tipo de equipamento / Serviço deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        //{name: "created", type: "myDateField", title: "Data de criação", width: 150, align: "center", readOnly: true},
        //{name: "updated", type: "myDateField", title: "Data de edição", width: 150, align: "center", readOnly: true},
        { type: "control" }
    ];

    return fields;
}

function GetDateField() {
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },

        itemTemplate: function(value) {
            var date = moment(value);
            return date.format("dddd, DD MMMM YYYY");
        },
    });
    return MyDateField;
}