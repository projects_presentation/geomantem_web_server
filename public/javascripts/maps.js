var currentInfoWindow;

var map;
var markersList = [];
var markerCluster;

function initMap() {

    map = new google.maps.Map(document.getElementById("map"));

    var styles = {
        hide: [{
                featureType: 'poi.business',
                stylers: [{ visibility: 'off' }]
            },
            {
                featureType: 'transit',
                elementType: 'labels.icon',
                stylers: [{ visibility: 'off' }]
            }
        ]
    };

    map.setOptions({ disableDoubleClickZoom: true, styles: styles['hide'] });

    setCustomZoomButton(map);

    var geoloccontrol = new klokantech.GeolocationControl(map, 18);

    addSearchBar(map);
}

function updateMap(data) {


    //Loop through all the markers and remove
    for (var i = 0; i < markersList.length; i++) {
        markersList[i].setMap(null);
        markersList[i] = null;
    }

    if (markerCluster) {
        markerCluster.clearMarkers();
    }

    markersList = [];

    if (data.length === 0) {
        centerOnCountry(map);
    } else {
        var bounds = new google.maps.LatLngBounds();
        $.each(data, function(index, value) {
            marker = GetMarker(value, map);
            markersList.push(marker);
            setMarkerListener(marker, value, map);
            bounds.extend(new google.maps.LatLng(value.warehouse_id === 0 ? value.latitude : value.warehouse_latitude, value.warehouse_id === 0 ? value.longitude : value.warehouse_longitude));
        });
        map.fitBounds(bounds);

    }
    markerCluster = new MarkerClusterer(map, markersList, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });



}

function setMarkerListener(marker, equipment, map) {

    google.maps.event.addListener(marker, 'click', function() {
        var infowindow = new google.maps.InfoWindow();
        var infoWindowContent = "";
        infoWindowContent += '<p><h3>Número de série: ' + equipment.serial_number + '</h3></p>';
        infoWindowContent += '<p>'
        if (equipment.photos_for_map.length == 0) {
            infoWindowContent += '<a class="example-image-link" href="images/no_image.png" data-lightbox="example-1"><img class="example-image" src="images/no_image.png" style="height: 150px;width: 150px"></a>'
        } else {
            for (var i = 0; i < equipment.photos_for_map.length; ++i) {
                infoWindowContent += '<a class="example-image-link" href="images/' + equipment.photos_for_map[i].filename + '" data-lightbox="example-1"><img class="example-image" src="images/' + equipment.photos_for_map[i].filename + '" style="height: 150px;width: 150px"></a>'
            }
        }
        infoWindowContent += '</p>'
        infoWindowContent += '<p>Data da ultima reparação: ' + moment(equipment.last_repair).format("YYYY-MM-DD HH:mm:ss") + '</p>';

        if (equipment.photos_for_map.length != 0) {
            infoWindowContent += '<p align="center">Observações após ultima reparação<p> ' + equipment.repair_observations + '</p></p>';
        }

        if (equipment.repairs_names.length != 0) {
            infoWindowContent += '<p align="center">Ultimas 10 reparações</p>';
            for (var i = 0; i < equipment.repairs_names.length; ++i) {
                infoWindowContent += '<p>' + equipment.repairs_names[i].repair_name + '</p>';
            }
            infoWindowContent += '<p align="center">**********************</p>';
        }
        //infoWindowContent += '<p><a class="example-image-link" href="images/' + equipment.photo_url + '" data-lightbox="example-1"><img class="example-image" src="images/' + equipment.photo_url + '" style="height: 150px;width: 150px"></a></p>';

        infoWindowContent += '<p>Tipo de equipamento / Serviço: ' + equipment.brand_name + '</p>';
        infoWindowContent += '<p>Nome / Marca / Modelo: ' + equipment.model_name + '</p>';
        infoWindowContent += '<p>Características: ' + equipment.characteristics + '</p>';
        infoWindowContent += '<button type="button" id="sendToWarehouse" class="btn btn-primary" style="margin-left: 10px" onclick="sendToWarehouse(\'' + equipment.id + '\',\'' + equipment.client_id + '\')">Enviar para o estaleiro</button>';
        infoWindowContent += '<button type="button" class="btn btn-primary" style="margin-left: 10px" onclick="replace(\'' + equipment.id + '\',\'' + equipment.client_id + '\')">Substituir</button>';
        infoWindowContent += '<button type="button" class="btn btn-danger pull-right" onclick="currentInfoWindow.close()">Fechar</button>';

        infowindow.setContent(infoWindowContent);
        if (currentInfoWindow !== null && currentInfoWindow !== undefined) {
            currentInfoWindow.close();
        }
        infowindow.open(map, marker);
        currentInfoWindow = infowindow;
    });

    if (equipment.warehouse_id === 0) {
        google.maps.event.addListener(marker, 'dragend', function(event) {
            equipment.latitude = this.getPosition().lat();
            equipment.longitude = this.getPosition().lng();
            getAddressByLatLong(equipment.latitude, equipment.longitude, function(address) {
                equipment.address = address;
                $.ajax({
                    type: "POST",
                    url: "/equipments/update",
                    data: equipment
                });
            });
        });
    }
}

function centerOnCountry(map) {
    var country = "Portugal";
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': country }, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            var reference = new google.maps.LatLng(latitude, longitude);
            map.setZoom(7);
            map.setCenter(reference);
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP);

        } else {
            alert("Could not find location: " + location);
        }
    });
}

function GetMarker(equipment, map) {
    var image = {};
    image.scaledSize = new google.maps.Size(25, 25);
    image.url = '/images/' + equipment.model_icon;
    var markerToReturn = new MarkerWithLabel({
        icon: image,
        position: new google.maps.LatLng(equipment.warehouse_id === 0 ? equipment.latitude : equipment.warehouse_latitude, equipment.warehouse_id === 0 ? equipment.longitude : equipment.warehouse_longitude),
        draggable: dragEquipments,
        map: map,
        title: ' Descrição: ' + equipment.serial_number,
        labelContent: equipment.serial_number,
        labelAnchor: new google.maps.Point(25, 50),
        labelClass: 'equipment_label' + equipment.status_id,
        labelInBackground: true
    });
    return markerToReturn;
}

function setCustomZoomButton(map) {

    // Create the DIV to hold the control and call the ZoomControl() constructor
    // passing in this DIV.
    var zoomControlDiv = document.createElement('div');

    // Creating divs & styles for custom zoom control
    zoomControlDiv.style.padding = '5px';

    // Set CSS for the control wrapper
    var controlWrapper = document.createElement('div');
    controlWrapper.style.backgroundColor = 'white';
    controlWrapper.style.borderStyle = 'solid';
    controlWrapper.style.borderColor = 'gray';
    controlWrapper.style.borderWidth = '1px';
    controlWrapper.style.cursor = 'pointer';
    controlWrapper.style.textAlign = 'center';
    controlWrapper.style.width = '150px';
    controlWrapper.style.height = '75px';
    controlWrapper.style.display = 'inline-flex';
    zoomControlDiv.appendChild(controlWrapper);

    // Set CSS for the zoomIn
    var zoomInButton = document.createElement('div');
    zoomInButton.style.width = '75px';
    zoomInButton.style.height = '75px';
    /* Change this to be the .png image you want to use */
    zoomInButton.style.backgroundImage = 'url("/images/zoom_in.png")';
    controlWrapper.appendChild(zoomInButton);

    // Set CSS for the zoomOut
    var zoomOutButton = document.createElement('div');
    zoomOutButton.style.width = '75px';
    zoomOutButton.style.height = '75px';
    /* Change this to be the .png image you want to use */
    zoomOutButton.style.backgroundImage = 'url("/images/zoom_out.png")';
    controlWrapper.appendChild(zoomOutButton);

    // Setup the click event listener - zoomIn
    google.maps.event.addDomListener(zoomInButton, 'click', function() {
        map.setZoom(map.getZoom() + 1);
    });

    // Setup the click event listener - zoomOut
    google.maps.event.addDomListener(zoomOutButton, 'click', function() {
        map.setZoom(map.getZoom() - 1);
    });

    zoomControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(zoomControlDiv);
}

function sendToWarehouse(equipmentID, clientID) {

    var esc = $.Event("keydown", { keyCode: 27 });
    $("map").trigger(esc);

    currentInfoWindow.close();

    var selectHTML = '<select id="selectedWarehouse" class="selectpicker" style="width:90%">';

    $.ajax({
        type: "GET",
        url: '/warehouses/get_available/',
        data: {
            client_id: clientID
        },
        success: function(data) {
            if (data.length === 0) {
                showErrorPopup("Enviar equipamento para estaleiro", "Não existem estaleiros disponíveis para este cliente");
            } else {
                data.forEach(function(element) {
                    selectHTML += '<option value=' + element.id + '>' + element.name + '</option>';
                }, this);
                selectHTML += '</select>';
                BootstrapDialog.show({
                    title: 'Enviar equipamento para o estaleiro',
                    message: $(selectHTML + '<script>$(".selectpicker").selectpicker();</script>'),
                    buttons: [{
                        label: 'Enviar para estaleiro',
                        cssClass: 'btn-primary',
                        hotkey: 13,
                        action: function(dialogRef) {
                            var selectedEquipment = document.getElementById('selectedWarehouse').value;

                            dialogRef.close();
                            $.ajax({
                                type: "POST",
                                url: '/equipments/update_warehouse/',
                                data: {
                                    equipmentID: equipmentID,
                                    newWarehouse: selectedEquipment
                                },
                                success: function(data) {
                                    if (data === true) {
                                        showOkPopup('Enviar equipamento para estaleiro', 'O equipamento foi enviado com sucesso para o estaleiro');
                                    } else {
                                        showErrorPopup('Enviar equipamento para estaleiro', 'O equipamemnto não foi enviado com sucesso para o estaleiro, por favor tente mais tarde...');
                                    }
                                    $("#equipmentsGrid").jsGrid("loadData");
                                },
                            });

                        }
                    }, {
                        label: 'Cancelar',
                        action: function(dialogRef) {
                            dialogRef.close();
                        }
                    }]
                });
            }
        },
    });
}

function replace(equipment, clientID) {
    currentInfoWindow.close();

    var selectHTML = '<h3>Seleccione o estaleiro</h3><p><select id="selectedWarehouse" class="selectpicker">';

    $.ajax({
        type: "GET",
        url: '/warehouses/get_available/',
        data: {
            client_id: clientID
        },
        success: function(listOfWarehouses) {
            if (listOfWarehouses.length === 0) {
                showErrorPopup("Substituir por equipamento no estaleiro", "Não existem estaleiros disponíveis para este cliente");
            } else {
                listOfWarehouses.forEach(function(element) {
                    selectHTML += '<option value=' + element.id + '>' + element.name + '</option>';
                }, this);
                selectHTML += '</select><p><h3>Seleccione o equipamento</h3><p><select id="selectedEquipment" class="selectpicker" style="margin-top: 25px;"></select>';

                BootstrapDialog.show({
                    title: 'Substituir por equipamento no estaleiro estaleiro',
                    message: $(selectHTML + '<script>$(".selectpicker").selectpicker(); $("#selectedWarehouse").on("change", function(){var selected = $(this).find("option:selected").val();fillAvailableEquipmentsToReplace(selected);}); fillAvailableEquipmentsToReplace(document.getElementById("selectedWarehouse").value)</script>'),
                    buttons: [{
                        label: 'Substituir',
                        cssClass: 'btn-primary',
                        hotkey: 13,
                        action: function(dialogRef) {
                            var selectedWarehouse = document.getElementById('selectedWarehouse').value;
                            var selectedEquipment = document.getElementById('selectedEquipment').value;

                            if (selectedEquipment === null || selectedEquipment === undefined) {
                                showErrorPopup('Substituição de equipamento', 'Por favor seleccione um equipamento para substituir');
                            } else {
                                dialogRef.close();
                                $.ajax({
                                    type: "POST",
                                    url: '/equipments/replace/',
                                    data: {
                                        equipmentAID: equipment,
                                        equipmentBID: selectedEquipment,
                                        selectedWarehouse: selectedWarehouse
                                    },
                                    success: function(data) {
                                        if (data === true) {
                                            showOkPopup('Substituição de equipamento', 'O equipamento foi substituido com sucesso');
                                        } else {
                                            showErrorPopup('Substituição de equipamento', 'O equipamemnto não foi substituido com sucesso, por favor tente mais tarde...');
                                        }
                                        $("#equipmentsGrid").jsGrid("loadData");
                                    },
                                });
                            }

                        }
                    }, {
                        label: 'Cancelar',
                        action: function(dialogRef) {
                            dialogRef.close();
                        }
                    }]
                });
            }
        },
    });
}

function fillAvailableEquipmentsToReplace(warehouseID) {
    var selectedEquipment = document.getElementById('selectedEquipment');
    selectedEquipment.innerHTML = '';
    $.ajax({
        type: "GET",
        url: '/equipments/get_all_in_warehouse/',
        data: {
            warehouse_id: warehouseID
        },
        success: function(data) {
            if (data.length === 0) {
                showErrorPopup("Substituir por equipamento no estaleiro", "Não existem equipamentos disponíveis para substituição");
            } else {
                data.forEach(function(element) {
                    selectedEquipment.innerHTML += '<option value=' + element.id + '>' + element.serial_number + '</option>';
                }, this);
                $('#selectedEquipment').selectpicker('refresh');
            }
        },
    });
}

function requestIntervention(elementID, serialNumber) {
    sendRequestSingle(elementID, serialNumber);
}

function getAddressByLatLong(latitude, longitude, address) {
    var geocoder = new google.maps.Geocoder;
    var latlng = { lat: latitude, lng: longitude };
    geocoder.geocode({ 'location': latlng }, function(results, status) {
        if (status === 'OK') {
            if (results[0]) {
                address(results[0].formatted_address);
            } else {
                address('Desconhecido');
            }
        } else {
            address('Desconhecido');
        }
    });
}


function addSearchBar(map) {
    var input = document.getElementById('searchInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {

        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            //window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setPosition(place.geometry.location);

    });
}