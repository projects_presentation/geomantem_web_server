var map;
var markersList = [];

function initMap() {

    map = new google.maps.Map(document.getElementById("map"));

    var styles = {
        hide: [{
                featureType: 'poi.business',
                stylers: [{ visibility: 'off' }]
            },
            {
                featureType: 'transit',
                elementType: 'labels.icon',
                stylers: [{ visibility: 'off' }]
            }
        ]
    };

    map.setOptions({ disableDoubleClickZoom: true, styles: styles['hide'] });

    setCustomZoomButton(map);

    var geoloccontrol = new klokantech.GeolocationControl(map, 18);

    addSearchBar(map);
}

function updateMap(data) {


    //Loop through all the markers and remove
    for (var i = 0; i < markersList.length; i++) {
        markersList[i].setMap(null);
    }
    markersList = [];

    if (data.length === 0) {
        centerOnCountry(map);
    } else {
        var bounds = new google.maps.LatLngBounds();
        $.each(data, function(index, value) {
            marker = GetMarker(value, map);
            markersList.push(marker);
            setMarkerClickListener(marker, value);
            bounds.extend(new google.maps.LatLng(value.warehouse_id === 0 ? value.latitude : value.warehouse_latitude, value.warehouse_id === 0 ? value.longitude : value.warehouse_longitude));
        });
        map.fitBounds(bounds);
    }

}

function setCustomZoomButton(map) {

    // Create the DIV to hold the control and call the ZoomControl() constructor
    // passing in this DIV.
    var zoomControlDiv = document.createElement('div');

    // Creating divs & styles for custom zoom control
    zoomControlDiv.style.padding = '5px';

    // Set CSS for the control wrapper
    var controlWrapper = document.createElement('div');
    controlWrapper.style.backgroundColor = 'white';
    controlWrapper.style.borderStyle = 'solid';
    controlWrapper.style.borderColor = 'gray';
    controlWrapper.style.borderWidth = '1px';
    controlWrapper.style.cursor = 'pointer';
    controlWrapper.style.textAlign = 'center';
    controlWrapper.style.width = '150px';
    controlWrapper.style.height = '75px';
    controlWrapper.style.display = 'inline-flex';
    zoomControlDiv.appendChild(controlWrapper);

    // Set CSS for the zoomIn
    var zoomInButton = document.createElement('div');
    zoomInButton.style.width = '75px';
    zoomInButton.style.height = '75px';
    /* Change this to be the .png image you want to use */
    zoomInButton.style.backgroundImage = 'url("/images/zoom_in.png")';
    controlWrapper.appendChild(zoomInButton);

    // Set CSS for the zoomOut
    var zoomOutButton = document.createElement('div');
    zoomOutButton.style.width = '75px';
    zoomOutButton.style.height = '75px';
    /* Change this to be the .png image you want to use */
    zoomOutButton.style.backgroundImage = 'url("/images/zoom_out.png")';
    controlWrapper.appendChild(zoomOutButton);

    // Setup the click event listener - zoomIn
    google.maps.event.addDomListener(zoomInButton, 'click', function() {
        map.setZoom(map.getZoom() + 1);
    });

    // Setup the click event listener - zoomOut
    google.maps.event.addDomListener(zoomOutButton, 'click', function() {
        map.setZoom(map.getZoom() - 1);
    });

    zoomControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(zoomControlDiv);
}

function setMarkerClickListener(marker, value) {
    marker.addListener('click', function() {
        startRoutePointRepairPopup(value);
    });
}

function centerOnCountry(map) {
    var country = "Portugal";
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': country }, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            var reference = new google.maps.LatLng(latitude, longitude);
            map.setZoom(7);
            map.setCenter(reference);
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP);

        } else {
            alert("Could not find location: " + location);
        }
    });
}

function GetMarker(equipment, map) {
    var image = {};
    image.scaledSize = new google.maps.Size(25, 25);
    image.url = '/images/' + equipment.model_icon;
    var markerToReturn = new MarkerWithLabel({
        icon: image,
        position: new google.maps.LatLng(equipment.warehouse_id === 0 ? equipment.latitude : equipment.warehouse_latitude, equipment.warehouse_id === 0 ? equipment.longitude : equipment.warehouse_longitude),
        map: map,
        title: ' Descrição: ' + equipment.serial_number,
        labelContent: equipment.serial_number,
        labelAnchor: new google.maps.Point(25, 50),
        labelClass: 'equipment_label' + equipment.status_id,
        labelInBackground: true
    });
    return markerToReturn;
}

function addSearchBar(map) {
    var input = document.getElementById('searchInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {

        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            //window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setPosition(place.geometry.location);

    });
}

function addSearchBar(map) {
    var input = document.getElementById('searchInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {

        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            //window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setPosition(place.geometry.location);

    });
}