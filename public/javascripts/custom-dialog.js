function showOkPopup(title, messageToShow) {
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: title,
        message: messageToShow,
        buttons: [{
            label: 'Ok',
            action: function(dialogRef) {
                dialogRef.close();
            }
        }]
    });
}

function showErrorPopup(title, messageToShow) {
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DANGER,
        title: title,
        message: messageToShow,
        buttons: [{
            label: 'Ok',
            action: function(dialogRef) {
                dialogRef.close();
            }
        }]
    });
}

function changeSettingsPopup(userId, username) {
    BootstrapDialog.show({
        title: 'A alterar as definições de ' + username,
        message: $('<h3>Nome do utilizador</h3><p><input type="text" class="form-control" placeholder="Nome" id="name" value="' + username + '"><p><h3>Password do utilizador</h3><p><input type="password" class="form-control" placeholder="Password" id="password"><p><input type="password" class="form-control" placeholder="Repita a password" id="repeatPassword">'),
        buttons: [{
            label: 'Alterar nome e password',
            cssClass: 'btn-primary',
            hotkey: 13,
            action: function(dialogRef) {
                var newName = document.getElementById('name').value;
                var password1 = document.getElementById('password').value;
                var password2 = document.getElementById('repeatPassword').value;

                if (newName.length < 5) {
                    showErrorPopup('Alterar dados de utilizador', 'O nome de utilizador deve estar preenchido e deve conter no mínimo 5 caracteres');
                    $("#name").val("");
                    return;
                }

                if (password1.length < 5 || password2.length < 5) {
                    showErrorPopup('Alterar dados de utilizador', 'As passwords devem estar preenchidas e devem conter no mínimo 5 caracteres');
                    $("#password1").val("");
                    $("#repeatPassword").val("");
                    return;
                }

                if (password1 !== password2) {
                    showErrorPopup('Alterar dados de utilizador', 'As passwords não são iguais');
                    $("#password").val("");
                    $("#repeatPassword").val("");
                    return;
                }
                dialogRef.close();
                $.ajax({
                    type: "POST",
                    url: '/accounts/settings/update_password',
                    data: {
                        id: userId,
                        name: document.getElementById('name').value,
                        password: password1
                    },
                    success: function(data) {
                        if (data === true) {
                            showOkPopup('Alterar dados de utilizador', 'Os dados do utilizador foram alterados com sucesso');
                        } else {
                            showErrorPopup('Alterar dados de utilizador', 'Erro a alterar dados do utilizador, por favor tente mais tarde...')
                        }

                    },
                });
            }
        }]
    });
}

function startRouteRepairPopup(route, jsgrid) {
    BootstrapDialog.show({
        title: 'Iniciar a reparação de: ' + route.serial_number,
        message: $('<h3>' + route.serial_number + '</h3>'),
        buttons: [{
            label: 'Iniciar reparação de rota',
            cssClass: 'btn-primary',
            hotkey: 13,
            action: function(dialogRef) {
                dialogRef.close();
                $.ajax({
                    type: "POST",
                    url: "/routes/update_started",
                    data: {
                        route: JSON.stringify(route)
                    },
                    success: function() {
                        showOkPopup('Iniciar reparação', 'Reparação iniciada com sucesso');
                        jsgrid.jsGrid("loadData");
                    },
                    error: function() {
                        showErrorPopup('Iniciar reparação', 'Reparação iniciada sem sucesso, por favor tente mais tarde...');
                        jsgrid.jsGrid("loadData");
                    }
                });

            }
        }, {
            label: 'Cancelar',
            cssClass: 'btn-danger',
            action: function(dialogRef) {
                dialogRef.close();
            }

        }]
    });
}

function endRouteRepairPopup(route, jsgrid) {
    BootstrapDialog.show({
        title: 'Terminar a reparação de: ' + route.serial_number,
        message: $('<h3>' + route.serial_number + '</h3>'),
        buttons: [{
            label: 'Terminar reparação de rota',
            cssClass: 'btn-primary',
            hotkey: 13,
            action: function(dialogRef) {
                dialogRef.close();
                $.ajax({
                    type: "POST",
                    url: "/routes/update_ended",
                    data: {
                        route: JSON.stringify(route)
                    },
                    success: function() {
                        showOkPopup('Terminar reparação', 'Reparação terminda com sucesso');
                        jsgrid.jsGrid("loadData");
                    },
                    error: function() {
                        showErrorPopup('Terminar reparação', 'Reparação terminada sem sucesso, por favor tente mais tarde...');
                        jsgrid.jsGrid("loadData");
                    }
                });

            }
        }, {
            label: 'Cancelar',
            cssClass: 'btn-danger',
            action: function(dialogRef) {
                dialogRef.close();
            }

        }]
    });
}

function startRoutePointRepairPopup(routePoint) {
    if (routePoint.ended !== null) {
        showErrorPopup('Iniciar reparação', 'Este ponto já se encontra com a reparação fechada!');
        return;
    }
    if (routePoint.started !== null) {
        window.location.href = "/equipment_repair?route_point_id=" + routePoint.route_point_id;
    } else {
        BootstrapDialog.show({
            title: 'Iniciar a reparação de: ' + routePoint.serial_number,
            message: $('<h3>' + routePoint.serial_number + '</h3><p><a class="example-image-link" href="images/' + routePoint.photo_url + '" data-lightbox="example-1"><img class="example-image" src="images/' + routePoint.photo_url + '" style="height: 150px;width: 150px"></a></p>'),
            buttons: [{
                label: 'Iniciar reparação',
                cssClass: 'btn-primary',
                hotkey: 13,
                action: function(dialogRef) {
                    dialogRef.close();
                    $.ajax({
                        type: "POST",
                        url: "/route_points/update_started",
                        data: {
                            routePoint: JSON.stringify(routePoint)
                        },
                        success: function() {
                            window.location.href = "/equipment_repair?route_point_id=" + routePoint.route_point_id;
                        }
                    });

                }
            }, {
                label: 'Cancelar',
                cssClass: 'btn-danger',
                action: function(dialogRef) {
                    dialogRef.close();
                }

            }, {
                label: 'Obter direcções',
                cssClass: 'btn-success pull-left',
                action: function(dialogRef) {
                    window.open('https://www.google.com/maps/search/?api=1&query=' + routePoint.latitude + ',' + routePoint.longitude);
                }

            }]
        });
    }
}

function endRoutePointRepairPopup(routePoint) {
    BootstrapDialog.show({
        title: 'Terminar a reparação de: ' + routePoint.serial_number,
        message: $('<h3>' + routePoint.serial_number + '</h3><p><a class="example-image-link" href="images/' + routePoint.photo_url + '" data-lightbox="example-1"><img class="example-image" src="images/' + routePoint.photo_url + '" style="height: 150px;width: 150px"></a></p>'),
        buttons: [{
            label: 'Terminar reparação',
            cssClass: 'btn-primary',
            hotkey: 13,
            action: function(dialogRef) {
                dialogRef.close();
                $.ajax({
                    type: "POST",
                    url: "/route_points/update_ended",
                    data: {
                        routePoint: JSON.stringify(routePoint)
                    },
                    success: function() {
                        showOkPopup('Terminar reparação', 'Reparação terminda com sucesso');
                        window.location.href = '/';
                    },
                    error: function() {
                        showErrorPopup('Terminar reparação', 'Reparação terminada sem sucesso, por favor tente mais tarde...');
                    }
                });

            }
        }, {
            label: 'Cancelar',
            cssClass: 'btn-danger',
            action: function(dialogRef) {
                dialogRef.close();
            }

        }]
    });
}

function createRoute(selectedItems) {
    BootstrapDialog.show({
        title: 'Criar nova rota',
        message: $('<h3>Nome da nova rota</h3><p><input type="text" class="form-control" placeholder="Nome da rota" id="name">'),
        buttons: [{
            label: 'Criar nova rota',
            cssClass: 'btn-primary',
            hotkey: 13,
            action: function(dialogRef) {
                var newName = document.getElementById('name').value;

                if (newName.length < 1 || newName.length >= 37) {
                    showErrorPopup('Erro ao criar nova rota', 'O nome da rota deve estar preenchido e deve conter no mínimo 1 caracter e no máximo 36 caracteres');
                    $("#name").val("");
                    return;
                }
                dialogRef.close();
                $.ajax({
                    type: "POST",
                    url: "/routes/insert",
                    data: {
                        routeName: newName,
                        routePoints: JSON.stringify(selectedItems)
                    },
                    success: function(data) {
                        if (data.insertRouteResult === true) {
                            showOkPopup('Criar nova rota', 'A nova rota foi criada com sucesso com o nome: ' + newName);
                        } else {
                            showErrorPopup('Erro ao criar nova rota', 'Existiu um erro ao criar a nova rota, por favor tente mais tarde!');
                        }

                    },
                });
            }
        }]
    });
}

function createWarehouse(latitude, longitude) {

    var selectHTML = '<select id="selectedClient" class="selectpicker">';

    $.ajax({
        type: "GET",
        url: '/clients/get/',
        success: function(data) {
            data.forEach(function(element) {
                selectHTML += '<option value=' + element.id + '>' + element.name + '</option>';
            }, this);
            selectHTML += '</select>'
            BootstrapDialog.show({
                title: 'Criar novo estaleiro',
                message: $('<h3>Nome do novo estaleiro</h3><p><input type="text" class="form-control" placeholder="Nome do estaleiro" id="name"><p><p>' + selectHTML + '<script>$(".selectpicker").selectpicker();</script>'),
                buttons: [{
                    label: 'Criar novo estaleiro',
                    cssClass: 'btn-primary',
                    hotkey: 13,
                    action: function(dialogRef) {
                        var newName = document.getElementById('name').value;
                        var selectedClient = document.getElementById('selectedClient').value;

                        if (newName.length < 1 || newName.length > 100) {
                            showErrorPopup('Erro ao criar novo estaleiro', 'O nome do estaleiro deve estar preenchido e deve conter no mínimo 1 caracter e no máximo 100 caracteres');
                            $("#name").val("");
                            return;
                        }
                        dialogRef.close();
                        $.ajax({
                            type: "POST",
                            url: "/warehouses/insert",
                            data: {
                                name: newName,
                                latitude: latitude,
                                longitude: longitude,
                                client_id: selectedClient
                            },
                            success: function(data) {
                                if (data.result === true) {
                                    showOkPopup('Criar novo estaleiro', 'A novo estaleiro foi criado com sucesso com o nome: ' + newName);
                                } else {
                                    showErrorPopup('Erro ao criar novo estaleiro', 'Existiu um erro ao criar o novo estaleiro, por favor tente mais tarde!');
                                }
                                $("#warehousesGrid").jsGrid("loadData");
                            },
                        });
                    }
                }]
            });
        },
    });
}

var clientAccount

function setClientAccount(account) {
    clientAccount = account;
}

function sendRequestSingle(elementID, serialNumber) {

    var selectHTML = '<select id="selectedEquipment" class="selectpicker"><option value=' + elementID + '>' + serialNumber + '</option></select>';
    showRequestBoxSingle(selectHTML);
}

function showRequestBoxSingle(selectHTML) {
    $.ajax({
        type: 'GET',
        url: '/requests/add_photo_to_request',
        success: function(addPhotoHTML) {
            BootstrapDialog.show({
                title: 'Enviar pedido de intervenção',
                message: $(selectHTML + '<p>' + addPhotoHTML + '<p><textarea placeholder="Introduza o pedido..." id="requestText" style="width: 100%;height: 100px; margin-top: 30px;"></textarea><script>$(".selectpicker").selectpicker();</script>'),
                buttons: [{
                    label: 'Enviar pedido',
                    cssClass: 'btn-primary',
                    hotkey: 13,
                    action: function(dialogRef) {
                        var requestText = document.getElementById('requestText').value;
                        var selectedEquipment = document.getElementById('selectedEquipment').value;
                        var serialNumber = document.getElementById('selectedEquipment').selectedOptions[0].innerHTML;

                        if (requestText.length > 500) {
                            showErrorPopup('Enviar pedido de intervenção', 'O pedido de intervenção não deve conter mais do que 500 caracteres');
                            $("#name").val("");
                            return;
                        }

                        dialogRef.close();
                        $.ajax({
                            type: "POST",
                            url: '/client_requests/insert/',
                            data: {
                                equipment_id: selectedEquipment,
                                text: requestText,
                                photo_url: uploadedFilename === undefined ? 'no_image.png' : uploadedFilename
                            },
                            success: function(data) {
                                if (data === true) {
                                    showOkPopup('Enviar pedido de intervenção', 'O pedido de intervenção foi enviado com sucesso');
                                    $.ajax({
                                        type: 'POST',
                                        url: '/email/send_email/',
                                        data: {
                                            to: 'admin@geomantem.pt',
                                            subject: 'Novo pedido de intervenção',
                                            text: 'Cliente: ' + clientAccount.username + '\n\nNúmero de série: ' + serialNumber + '\n\nTexto do pedido: ' + requestText
                                        }
                                    });
                                } else {
                                    showErrorPopup('Enviar pedido de intervenção', 'O pedido de intervenção não foi enviado com sucesso, por favor tente mais tarde...');
                                }

                            },
                        });

                    }
                }, {
                    label: 'Cancelar',
                    action: function(dialogRef) {
                        dialogRef.close();
                    }
                }]
            });
        }
    });
}