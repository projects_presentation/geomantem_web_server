function downloadButtonClick() {
    var listFromTable = $("#clientGrid").jsGrid("option", "data");
    var filteredDataList = [];
    for (var i = 0; i < listFromTable.length; ++i) {
        filteredDataList.push({
            "Número de série": listFromTable[i].serial_number,
            "Características": listFromTable[i].characteristics,
            "Tipo de equipamento / Serviço": listFromTable[i].brand_name,
            "Nome / Marca / Modelo": listFromTable[i].model_name,
            "Estado após reparação": listFromTable[i].status_name,
            "Endereço": listFromTable[i].address,
            "Observações": listFromTable[i].observations,
            "Estaleiro": listFromTable[i].warehouse_name,
            "Substituiu": listFromTable[i].replaced_serial_number,
            "Ultima Intervenção": listFromTable[i].replaced_serial_number
        });
    }
    alasql("SELECT * INTO XLSX('relatorio.xlsx',{headers:true}) FROM ? ", [filteredDataList]);
}