function fillTechniciansAccountsTable() {

        $("#clientsAccountsGrid").jsGrid({
            width: "90%",
            filtering: true,
            inserting: true,
            editing: true,
            sorting: true,
            paging: true,
            autoload: true,
            pageSize: 10,
            pageButtonCount: 5,

            deleteConfirm: "Deseja realmente apagar a conta de técnico?",
            invalidMessage: "Dados de preenchimento são inválidos",

            controller: {
                loadData: function (filter) {
                    return $.ajax({
                        type: "GET",
                        url: "/technicians_accounts/get",
                        data: filter
                    });
                },
                insertItem: function (item) {
                    return $.ajax({
                        type: "POST",
                        url: "/technicians_accounts/insert",
                        data: item
                    });
                },
                updateItem: function (item) {
                    return $.ajax({
                        type: "POST",
                        url: "/technicians_accounts/update",
                        data: item
                    });
                },
                deleteItem: function (item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/technicians_accounts/delete",
                        data: item
                    });
                }
            },

            fields: getTableFields()
        });
};

function getTableFields() {
    var fields = [
        {
            name: "username",
            type: "text",
            title: "Nome do utilizador",
            align: "center",
            validate: {
                message: function (value, item) {
                    return "O nome do técnico deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function (value, item) {
                    return value.toString().length > 0 && value.toString().length <=100;
                }
            }
        },
        {
            name: "email",
            type: "text",
            title: "Email",
            align: "center",
            validate: {
                message: function (value, item) {
                    return "O email do técnico deve conter pelo menos 1 caracter e no máximo 255. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function (value, item) {
                    return value.toString().length > 0 && value.toString().length <=255;
                }
            }
        },
        {type: "control"}
    ];

    return fields;
}

