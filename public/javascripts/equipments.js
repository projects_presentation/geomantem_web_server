var dragEquipments = false;
var selectedLocation = null;
var selectedMarker = null;

function fillEquipmentsTable() {

    jsGrid.fields.myDateField = getDateField();

    $('#dragEquipmentsCheckBox').change(function() {
        dragEquipments = $(this).prop('checked');
        updateMap($("#equipmentsGrid").jsGrid("option", "data"));
    });

    $.ajax({
        type: "GET",
        url: "/statuses/get"
    }).done(function(statuses) {
        $.ajax({
            type: "GET",
            url: "/clients/get"
        }).done(function(clients) {
            $.ajax({
                type: "GET",
                url: "/brands/get"
            }).done(function(brands) {
                $.ajax({
                    type: "GET",
                    url: "/models/get"
                }).done(function(models) {
                    $.ajax({
                        type: "GET",
                        url: "/warehouses/getall"
                    }).done(function(warehouses) {
                        statuses.unshift({ id: "0", name: "" });
                        clients.unshift({ id: "0", name: "" });
                        brands.unshift({ id: "0", name: "" });
                        models.unshift({ id: "0", name: "" });
                        warehouses.unshift({ id: "-1", name: "" });

                        $("#equipmentsGrid").jsGrid({
                            width: "90%",
                            filtering: true,
                            inserting: true,
                            editing: true,
                            sorting: true,
                            paging: true,
                            autoload: false,
                            pageSize: 50,
                            pageButtonCount: 5,

                            deleteConfirm: "Deseja realmente apagar o equipamento?",
                            invalidMessage: "Dados de preenchimento são inválidos",

                            rowClick: function(args) {
                                showEquipmentInfo(args.item);
                            },

                            onItemInserted: function(args) {
                                showSelectEquipmentLocation(args.item);
                            },

                            onItemInserting: function(args) {
                                args.item.latitude = 38.931128;
                                args.item.longitude = -9.247341;
                            },

                            onItemUpdated: function(args) {
                                updateMap(args.grid.data);
                                var modelsField = args.grid.fields[3];
                                modelsField.items = models;
                                $(".models-insert-css").empty().append(modelsField.insertTemplate());
                            },

                            onRefreshed: function(args) {
                                updateMap(args.grid.data);
                                var modelsField = args.grid.fields[3];
                                modelsField.items = models;
                                $(".models-insert-css").empty().append(modelsField.insertTemplate());
                            },

                            controller: {
                                loadData: function(filter) {
                                    return $.ajax({
                                        type: "GET",
                                        url: "/equipments/get_formated",
                                        data: filter
                                    });
                                },
                                insertItem: function(item) {
                                    return $.ajax({
                                        type: "POST",
                                        url: "/equipments/insert",
                                        data: item
                                    });
                                },
                                updateItem: function(item) {
                                    return $.ajax({
                                        type: "POST",
                                        url: "/equipments/update",
                                        data: item
                                    });
                                },
                                deleteItem: function(item) {
                                    return $.ajax({
                                        type: "DELETE",
                                        url: "/equipments/delete",
                                        data: item
                                    });
                                }
                            },

                            fields: getTableFields(statuses, clients, brands, models, warehouses)
                        });
                    });
                });

            });
        });
    });
};

function getTableFields(statuses, clients, brands, models, warehouses) {
    var fields = [{
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O número de série do equipamento deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        {
            name: "characteristics",
            type: "text",
            title: "Características",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "As características do equipamento não devem conter mais do que 500 caracteres. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length <= 500;
                }
            }
        },
        {
            name: "brand_id",
            type: "select",
            title: "Tipo de equipamento / Serviço",
            items: brands,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo Tipo de equipamento / Serviço não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.brand_id !== 0;
                },
            },
            filterTemplate: function() {
                var modelsField = this._grid.fields[3];
                modelsField.selectedIndex = 0;
                var $filterControl = jsGrid.fields.select.prototype.filterTemplate.call(this);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-filter-css").empty().append(modelsField.filterTemplate());
                });
                return $filterControl;
            },
            insertTemplate: function() {
                var modelsField = this._grid.fields[3];
                modelsField.selectedIndex = 0;
                var $filterControl = jsGrid.fields.select.prototype.insertTemplate.call(this);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-insert-css").empty().append(modelsField.insertTemplate());
                });
                return $filterControl;
            },
            editTemplate: function(item, value) {
                var modelsField = this._grid.fields[3];
                var $filterControl = jsGrid.fields.select.prototype.editTemplate.call(this, item, value);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-edit-css").empty().append(modelsField.editTemplate());
                });
                return $filterControl;
            }
        },
        {
            name: "model_id",
            type: "select",
            title: "Nome / Marca / Modelo",
            items: models,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo Nome / Marca / Modelo não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.model_id !== 0;
                },
            },
            filtercss: 'models-filter-css',
            insertcss: 'models-insert-css',
            editcss: 'models-edit-css'
        },
        {
            name: "model_icon",
            title: 'Icon',
            align: 'center',
            itemTemplate: function(val, item) {
                return $("<img>").attr("src", '/equipments/images/' + val).css({ height: 50, width: 50 });
            },
        },
        {
            name: "client_id",
            type: "select",
            title: "Cliente",
            items: clients,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo cliente não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.client_id !== 0;
                },
            },
        },
        {
            name: "status_id",
            type: "select",
            title: "Estado",
            items: statuses,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo estado não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.status_id !== 0;
                },
            },
        },
        {
            name: "observations",
            type: "text",
            title: "Observações",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "As observações do equipamento não devem conter mais do que 500 caracteres. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length <= 500;
                }
            }
        },
        {
            name: "address",
            type: "text",
            title: "Endereço",
            align: "center"
        },
        {
            name: "warehouse_id",
            type: "select",
            title: "Estaleiro",
            items: warehouses,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo estaleiro não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.warehouse_id !== -1;
                },
            },
        },
        { name: "last_repair", type: "myDateField", title: "Ultima intervenção", width: 150, align: "center", readOnly: true },
        {
            name: "replaced_serial_number",
            type: "text",
            title: "Substituiu",
            align: "center"
        },
        {
            name: "photo_url",
            type: "text",
            title: "Imagem",
            align: "center",
            visible: false,
        },
        { type: "control" }
    ];

    return fields;
}

function getDateField() {
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function(value) {
            var date = moment(value);

            var dateToReturn = date.format("YYYY-MM-DD HH:mm:ss");
            if (dateToReturn.indexOf("Invalid") !== -1) {
                dateToReturn = 'Desconhecida';
            }
            return dateToReturn;
        },
    });
    return MyDateField;
}

function showEquipmentInfo(equipment) {
    var infoWindowContent = "";
    infoWindowContent += '<p><h3>Número de série: ' + equipment.serial_number + '</h3></p>';
    infoWindowContent += '<p>'
    if (equipment.photos_for_map.length == 0) {
        infoWindowContent += '<a class="example-image-link" href="images/no_image.png" data-lightbox="example-1"><img class="example-image" src="images/no_image.png" style="height: 150px;width: 150px"></a>'
    } else {
        for (var i = 0; i < equipment.photos_for_map.length; ++i) {
            infoWindowContent += '<a class="example-image-link" href="images/' + equipment.photos_for_map[i].filename + '" data-lightbox="example-1"><img class="example-image" src="images/' + equipment.photos_for_map[i].filename + '" style="height: 150px;width: 150px"></a>'
        }
    }
    infoWindowContent += '</p>'
    infoWindowContent += '<p>Data da ultima reparação: ' + moment(equipment.last_repair).format("YYYY-MM-DD HH:mm:ss") + '</p>';

    if (equipment.photos_for_map.length != 0) {
        infoWindowContent += '<p align="center">Observações após ultima reparação<p> ' + equipment.repair_observations + '</p></p>';
    }

    if (equipment.repairs_names.length != 0) {
        infoWindowContent += '<p align="center">Ultimas 10 reparações</p>';
        for (var i = 0; i < equipment.repairs_names.length; ++i) {
            infoWindowContent += '<p>' + equipment.repairs_names[i].repair_name + '</p>';
        }
        infoWindowContent += '<p align="center">**********************</p>';
    }

    infoWindowContent += '<p>Tipo de equipamento / Serviço: ' + equipment.brand_name + '</p>';
    infoWindowContent += '<p>Nome / Marca / Modelo: ' + equipment.model_name + '</p>';
    infoWindowContent += '<p>Características: ' + equipment.characteristics + '</p>';

    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Equipamento: ' + equipment.serial_number,
        message: infoWindowContent,
        buttons: [{
            label: 'Ok',
            action: function(dialogRef) {
                dialogRef.close();
            }
        }]
    });
}

function showSelectEquipmentLocation(equipment) {
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Seleccione a localização para o novo equipamento',
        message: '<div id="mapToSelectLocation"></div><input id="searchInputSmallMap" class="controls" type="text" placeholder="Introduza a localização..."><script>createMapToSelectLocation()</script>',
        buttons: [{
            label: 'Ok',
            action: function(dialogRef) {
                if (selectedLocation === null) {
                    showErrorPopup('Localização de equipamento', 'A localização do equipamento ainda não foi definida, por favor faça duplo clique sobre o mapa para a definir');
                } else {
                    equipment.latitude = selectedLocation.lat();
                    equipment.longitude = selectedLocation.lng();
                    getAddressByLatLong(equipment.latitude, equipment.longitude, function(address) {
                        equipment.address = address;
                        $.ajax({
                            type: "POST",
                            url: "/equipments/update",
                            data: equipment,
                            success: function(data) {
                                if (data !== null || data !== undefined) {
                                    showOkPopup('Localização de equipamento', 'A localização do equipamento foi definida com sucesso');
                                } else {
                                    showErrorPopup('Localização de equipamento', 'A localização do equipamento foi definida sem sucesso por favor tente mais tarde...');
                                }
                                $("#equipmentsGrid").jsGrid("loadData");
                            },
                        });
                        dialogRef.close();
                    });
                }
            }
        }]
    });
}

function createMapToSelectLocation() {

    var map = new google.maps.Map(document.getElementById("mapToSelectLocation"));

    var styles = {
        hide: [{
                featureType: 'poi.business',
                stylers: [{ visibility: 'off' }]
            },
            {
                featureType: 'transit',
                elementType: 'labels.icon',
                stylers: [{ visibility: 'off' }]
            }
        ]
    };

    map.setOptions({ disableDoubleClickZoom: true, styles: styles['hide'] });

    map.addListener('dblclick', function(event) {
        selectedLocation = event.latLng;
        if (selectedMarker !== null) {
            selectedMarker.setMap(null);
        }
        selectedMarker = new google.maps.Marker({
            position: selectedLocation,
            map: map
        });
        map.panTo(selectedLocation);
    });

    centerOnCountry(map);
    var geoloccontrol = new klokantech.GeolocationControl(map, 18);
    addSearchBarToSmallMap(map);
}

function addSearchBarToSmallMap(map) {
    var input = document.getElementById('searchInputSmallMap');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {

        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            //window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setPosition(place.geometry.location);

    });
}