function fillClientsAccountsTable() {

    $.ajax({
        type: "GET",
        url: "/clients/get"
    }).done(function(clients) {

        clients.unshift({ id: "0", name: "" });

        $("#clientsAccountsGrid").jsGrid({
            width: "90%",
            filtering: true,
            inserting: true,
            editing: true,
            sorting: true,
            paging: true,
            autoload: true,
            pageSize: 10,
            pageButtonCount: 5,

            deleteConfirm: "Deseja realmente apagar a conta de utilizador?",
            invalidMessage: "Dados de preenchimento são inválidos",

            controller: {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        url: "/clients_accounts/get",
                        data: filter
                    });
                },
                insertItem: function(item) {
                    return $.ajax({
                        type: "POST",
                        url: "/clients_accounts/insert",
                        data: item
                    });
                },
                updateItem: function(item) {
                    return $.ajax({
                        type: "POST",
                        url: "/clients_accounts/update",
                        data: item
                    });
                },
                deleteItem: function(item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/clients_accounts/delete",
                        data: item
                    });
                }
            },

            fields: getTableFields(clients)
        });
    });
};

function getTableFields(clients) {
    var fields = [{
            name: "username",
            type: "text",
            title: "Nome do utilizador",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O nome do utilizador deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        {
            name: "email",
            type: "text",
            title: "Email",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O email do utilizador deve conter pelo menos 1 caracter e no máximo 255. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 255;
                }
            }
        },
        {
            name: "client_id",
            type: "select",
            title: "Cliente",
            items: clients,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo cliente não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.client_id !== 0;
                },
            },
        },
        {
            name: "authorize_repairs",
            type: "checkbox",
            title: "Autoriza reparações?",
            sorting: false
        },
        { type: "control" }
    ];

    return fields;
}