function fillReportsTable(route_id_for_report) {

    jsGrid.fields.myDateFieldStart = getDateField(true);
    jsGrid.fields.myDateFieldEnd = getDateField(false);

    $("#reportsGrid").jsGrid({
        width: "90%",
        filtering: false,
        inserting: false,
        editing: false,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 30,
        pageButtonCount: 5,

        controller: {
            loadData: function(filter) {
                filter.route_id = route_id_for_report;
                return $.ajax({
                    type: "GET",
                    url: "/reports/get",
                    data: filter
                });
            }
        },

        fields: getTableFieldsForReport()
    });
};

function getTableFieldsForReport() {
    var fields = [{
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center"
        },
        { name: "started", type: "myDateFieldStart", title: "Iniciou reparação", align: "center", readOnly: true },
        { name: "ended", type: "myDateFieldEnd", title: "Terminou reparação", align: "center", readOnly: true },
        {
            name: "repair_name",
            type: "text",
            title: "Reparações efectuadas",
            align: "center"
        },
        {
            name: "brand_name",
            type: "text",
            title: "Tipo de equipamento / Serviço",
            align: "center"
        },
        {
            name: "model_name",
            type: "text",
            title: "Nome / Marca / Modelo",
            align: "center"
        },
        {
            name: "status_name",
            type: "text",
            title: "Estado após reparação",
            align: "center"
        }
    ];

    return fields;
}

function getDateField(start) {
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function(value) {
            var date = moment(value);

            var dateToReturn = date.format("YYYY-MM-DD HH:mm:ss");
            if (dateToReturn.indexOf("Invalid") !== -1) {
                dateToReturn = (start === true ? "Não iniciou reparação" : " Não terminou reparação");
            }
            return dateToReturn;
        },
    });
    return MyDateField;
}