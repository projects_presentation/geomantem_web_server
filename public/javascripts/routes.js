var dragEquipments = false;
var itemsSelected = false;

function fillEquipmentsTable() {

    $('#generateRouteButton').attr('onClick', 'generateNewRoute();');

    $.ajax({
        type: "GET",
        url: "/statuses/get"
    }).done(function(statuses) {
        $.ajax({
            type: "GET",
            url: "/clients/get"
        }).done(function(clients) {
            $.ajax({
                type: "GET",
                url: "/brands/get"
            }).done(function(brands) {
                $.ajax({
                    type: "GET",
                    url: "/models/get"
                }).done(function(models) {
                    $.ajax({
                        type: "GET",
                        url: "/warehouses/getall"
                    }).done(function(warehouses) {
                        statuses.unshift({ id: "0", name: "" });
                        clients.unshift({ id: "0", name: "" });
                        brands.unshift({ id: "0", name: "" });
                        models.unshift({ id: "0", name: "" });
                        warehouses.unshift({ id: "-1", name: "" });

                        $("#equipmentsGrid").jsGrid({
                            width: "90%",
                            filtering: true,
                            inserting: false,
                            editing: false,
                            sorting: true,
                            paging: true,
                            autoload: true,
                            pageSize: 10,
                            pageButtonCount: 5,

                            deleteConfirm: "Deseja realmente apagar o equipamento?",
                            invalidMessage: "Dados de preenchimento são inválidos",

                            onItemInserting: function(args) {
                                args.item.latitude = 38.931128;
                                args.item.longitude = -9.247341;
                            },

                            onRefreshed: function(args) {
                                updateMap(args.grid.data);
                                var modelsField = args.grid.fields[3];
                                modelsField.items = models;
                                $(".models-insert-css").empty().append(modelsField.insertTemplate());
                            },

                            rowClick: function(item) {
                                if (item.event.target.type == "checkbox")
                                    return;
                                $(item.event.target).closest("tr").find("input[type=checkbox]").click();
                            },

                            controller: {
                                loadData: function(filter) {
                                    return $.ajax({
                                        type: "GET",
                                        url: "/equipments/get_formated",
                                        data: filter
                                    });
                                },
                                insertItem: function(item) {
                                    return $.ajax({
                                        type: "POST",
                                        url: "/equipments/insert",
                                        data: item
                                    });
                                },
                                updateItem: function(item) {
                                    return $.ajax({
                                        type: "POST",
                                        url: "/equipments/update",
                                        data: item
                                    });
                                },
                                deleteItem: function(item) {
                                    return $.ajax({
                                        type: "DELETE",
                                        url: "/equipments/delete",
                                        data: item
                                    });
                                }
                            },

                            fields: getTableFields(statuses, clients, brands, models, warehouses)
                        });

                    });

                });

            });
        });
    });
};

function getTableFields(statuses, clients, brands, models, warehouses) {
    var fields = [{
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O número de série do equipamento deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        {
            name: "characteristics",
            type: "text",
            title: "Características",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "As características do equipamento não devem conter mais do que 500 caracteres. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length <= 500;
                }
            }
        },
        {
            name: "brand_id",
            type: "select",
            title: "Tipo de equipamento / Serviço",
            items: brands,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo Tipo de equipamento / Serviço não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.brand_id !== 0;
                },
            },
            filterTemplate: function() {
                var modelsField = this._grid.fields[3];
                modelsField.selectedIndex = 0;
                var $filterControl = jsGrid.fields.select.prototype.filterTemplate.call(this);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-filter-css").empty().append(modelsField.filterTemplate());
                });
                return $filterControl;
            },
            insertTemplate: function() {
                var modelsField = this._grid.fields[3];
                modelsField.selectedIndex = 0;
                var $filterControl = jsGrid.fields.select.prototype.insertTemplate.call(this);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-insert-css").empty().append(modelsField.insertTemplate());
                });
                return $filterControl;
            },
            editTemplate: function(item, value) {
                var modelsField = this._grid.fields[3];
                var $filterControl = jsGrid.fields.select.prototype.editTemplate.call(this, item, value);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-edit-css").empty().append(modelsField.editTemplate());
                });
                return $filterControl;
            }
        },
        {
            name: "model_id",
            type: "select",
            title: "Nome / Marca / Modelo",
            items: models,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo Nome / Marca / Modelo não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.model_id !== 0;
                },
            },
            filtercss: 'models-filter-css',
            insertcss: 'models-insert-css',
            editcss: 'models-edit-css'
        },
        {
            name: "client_id",
            type: "select",
            title: "Cliente",
            items: clients,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo cliente não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.client_id !== 0;
                },
            },
        },
        {
            name: "status_id",
            type: "select",
            title: "Estado",
            items: statuses,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo estado não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.status_id !== 0;
                },
            },
        },
        {
            name: "observations",
            type: "text",
            title: "Observações",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "As observações do equipamento não devem conter mais do que 500 caracteres. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length <= 500;
                }
            }
        },
        {
            name: "warehouse_id",
            type: "select",
            title: "Estaleiro",
            items: warehouses,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo estaleiro não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.warehouse_id !== -1;
                },
            },
        },
        {
            name: "checked",
            type: "checkbox",
            title: "Seleccionar para rota",
            sorting: false,
            headerTemplate: function() {
                return $("<button>").attr("type", "button").text("Seleccionar tudo")
                    .on("click", function() {
                        var listFromTable = $("#equipmentsGrid").jsGrid("option", "data");
                        itemsSelected = !itemsSelected;
                        selectedItems = [];
                        for (var i = 0; i < listFromTable.length; ++i) {
                            itemsSelected ? selectItem(listFromTable[i]) : unselectItem(listFromTable[i]);
                        }
                        $('input').prop('checked', itemsSelected);
                    });
            },
            itemTemplate: function(_, item) {
                return $("<input>").attr("type", "checkbox")
                    .prop("checked", $.inArray(item, selectedItems) > -1)
                    .on("change", function(e) {
                        $(this).is(":checked") ? selectItem(item) : unselectItem(item);
                    });
            },
        },
        { type: "control", deleteButton: false, editButton: false }
    ];

    return fields;
}

var selectedItems = [];

var selectItem = function(item) {
    selectedItems.push(item);
    console.log(selectedItems);
};

var unselectItem = function(item) {
    for (var i = 0; i < selectedItems.length; ++i) {
        if (item.id === selectedItems[i].id) {
            selectedItems.splice(i, 1);
        }
    }
    console.log(selectedItems);
};

function generateNewRoute() {
    if (selectedItems.length === 0) {
        showErrorPopup("Erro ao criar rota", "Por favor seleccione pelo menos um equipamento para gerar a nova rota!");
    } else {
        createRoute(selectedItems);
    }

}