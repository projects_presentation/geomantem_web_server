function fillClientsTable() {

    $("#clientsGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,

        deleteConfirm: "Deseja realmente apagar o cliente?",
        invalidMessage: "Dados de preenchimento são inválidos",

        controller: {
            loadData: function (filter) {
                return $.ajax({
                    type: "GET",
                    url: "/clients/get",
                    data: filter
                });
            },
            insertItem: function (item) {
                return $.ajax({
                    type: "POST",
                    url: "/clients/insert",
                    data: item
                });
            },
            updateItem: function (item) {
                return $.ajax({
                    type: "POST",
                    url: "/clients/update",
                    data: item
                });
            },
            deleteItem: function (item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/clients/delete",
                    data: item
                });
            }
        },

        fields: getTableFields()
    });
};

function getTableFields() {
    var fields = [
        {
            name: "name",
            type: "text",
            title: "Nome",
            align: "center",
            validate: {
                message: function (value, item) {
                    return "O nome do cliente deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function (value, item) {
                    return value.toString().length > 0 && value.toString().length <=100;
                }
            }
        },
        {
            name: "address",
            type: "text",
            title: "Endereço",
            align: "center",
            validate: {
                message: function (value, item) {
                    return "O endereço do cliente deve conter pelo menos 1 caracter e no máximo 500. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function (value, item) {
                    return value.toString().length > 0 && value.toString().length <= 500;
                }
            }
        },
        {type: "control"}
    ];

    return fields;
}

