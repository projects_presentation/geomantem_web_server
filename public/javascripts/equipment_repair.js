var selectedStatus = 0;

function fillStatusesDropdown() {
    $.get("/statuses/get", function(data) {
        var dropdown = document.getElementById('selectedStatus');
        dropdown.innerHTML = '';
        var html = '';
        for (var i = 0; i < data.length; ++i) {
            html += '<li><a onclick="setSelectedStatus(\'' + data[i].name + '\',\'' + data[i].id + '\')"> ' + data[i].name + '</a></li>';

        }
        dropdown.innerHTML = html;
        $(".dropdown-menu li a").click(function() {
            $(this).parents(".btn-group").find('#status-selection-options').html($(this).text() + ' <span class="caret"></span>');
            $(this).parents(".btn-group").find('#status-selection-options').val($(this).data('value'));
        });
    });
}

function setSelectedStatus(statusName, statusId) {
    selectedStatus = statusId;
}

function fillEquipmentRepairsTable(routePoint) {

    $('#terminateRepairButton').attr('onClick', 'terminateRepair(routePoint)');

    $.ajax({
        type: "GET",
        url: "/equipment_repair/get_repairs_list"
    }).done(function(repairs) {

        $("#equipmentRepairsGrid").jsGrid({
            width: "90%",
            filtering: false,
            inserting: false,
            editing: true,
            sorting: true,
            paging: true,
            autoload: true,
            pageSize: 10,
            pageButtonCount: 5,

            rowClick: function(args) {
                $(args.event.target).closest("tr").find("input[type=checkbox]").click();
            },

            controller: {
                loadData: function(filter) {
                    filter.model_id = routePoint.model_id;
                    return $.ajax({
                        type: "GET",
                        url: "/equipment_repair/get",
                        data: filter
                    });
                }
            },

            fields: getEquipmentsRepairsTableFields(repairs)
        });
    });

};

function getEquipmentsRepairsTableFields(repairs) {
    var fields = [{
            name: "repair_id",
            type: "select",
            title: "Descrição",
            items: repairs,
            valueField: "id",
            valueType: "number",
            textField: "description",
            selectedIndex: 0,
            readOnly: true
        },
        {
            name: "checked",
            type: "checkbox",
            title: "Aplicada a Nome / Marca / Modelo",
            sorting: false,
            itemTemplate: function(_, item) {
                return $("<input>").attr("type", "checkbox")
                    .on("click", function(e) {
                        $(this).is(":checked") != $(this).is(":checked");
                        item.checked = $(this).is(":checked");
                        e.stopPropagation();
                    });
            },
        }
    ];

    return fields;
}

function updateModelsRepairsTable(data, routePoint) {
    $.ajax({
        type: "POST",
        url: "/equipment_repair/update",
        data: {
            list: JSON.stringify(data),
            route_point_id: routePoint.route_point_id
        }
    }).done(function() {

    });
}

function terminateRepair(routePoint) {
    if (selectedStatus === 0) {
        showErrorPopup('Terminar reparação', 'Por favor indique em que estado se encontra o equipamento!');
    } else {
        var data = $("#equipmentRepairsGrid").jsGrid("option", "data");
        updateModelsRepairsTable(data, routePoint);
        routePoint.observations = document.getElementById('observations').value;
        routePoint.status_id = selectedStatus;
        endRoutePointRepairPopup(routePoint);
    }

}