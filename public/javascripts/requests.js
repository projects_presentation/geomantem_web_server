var selectedRequest;

function fillRequestsTable() {

    jsGrid.fields.myDateField = getDateField();

    $("#requestsGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: true,
        editing: false,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,

        deleteConfirm: "Deseja realmente apagar o pedido de intrevenção?",
        invalidMessage: "Dados de preenchimento são inválidos",

        rowClass: function(item, itemIndex) {
            if (item.status_id === 5) {
                return 'selected-row-orange';
            } else {
                return item.ended === null ? "selected-row-red" : "selected-row-green";
            }

        },

        rowClick: function(args) {
            selectedRequest = args.item;
            if (selectedRequest.status_id === 5) {
                showErrorPopup('Enviar resposta ao cliente', 'Este equipamento ainda aguarda pedido de autorização de reparação');
                return;
            }
            if (selectedRequest.ended === null) {
                document.getElementById('response_helper').innerHTML = 'Responder ao pedido Nº: ' + selectedRequest.id;
                document.getElementById('response_div').style.display = 'block';
            } else {
                showErrorPopup('Enviar resposta ao cliente', 'Já foi enviada uma resposta a este cliente');
            }
        },

        controller: {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "/requests/get",
                    data: filter
                });
            },
            insertItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "/requests/insert",
                    data: item
                });
            },
            updateItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "/requests/update",
                    data: item
                });
            },
            deleteItem: function(item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/requests/delete",
                    data: item
                });
            }
        },

        fields: getTableFields()
    });
};

function getTableFields() {
    var fields = [{
            name: "id",
            type: "text",
            title: "Número de pedido",
            align: "center",
        },
        {
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center",
        },
        {
            name: "text",
            type: "text",
            title: "Pedido",
            align: "center",
        },
        {
            title: 'Imagem',
            name: "photo_url",
            itemTemplate: function(val, item) {
                return $("<img>").attr("src", '/equipments/images/' + val).css({ height: 50, width: 50 }).on("click", function() {
                    $("#imagePreview").attr("src", item.photo_url);
                    $("#dialog").dialog("open");
                });
            },
            align: "center",
            width: 120
        },
        {
            name: "client_name",
            type: "text",
            title: "Cliente",
            align: "center",
        },
        { name: "started", type: "myDateField", title: "Data do pedido", width: 150, align: "center", readOnly: true },
        {
            name: "response",
            type: "text",
            title: "Resposta enviada ao cliente",
            align: "center"
        },
        { name: "ended", type: "myDateField", title: "Data de fecho do pedido", width: 150, align: "center", readOnly: true },
        { type: "control", editButton: false, deleteButton: false, inserting: false }
    ];

    return fields;
}

function getDateField() {
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function(value) {
            var date = moment(value);

            var dateToReturn = date.format("YYYY-MM-DD HH:mm:ss");
            if (dateToReturn.indexOf("Invalid") !== -1) {
                dateToReturn = 'Ainda não obteve resposta';
            }
            return dateToReturn;
        },
    });
    return MyDateField;
}

function sendResponseToClient() {
    var responseText = document.getElementById("response").value;
    if (responseText.length > 500) {
        showErrorPopup('Erro a enviar resposta', 'O texto da resposta não deve conter mais do que 500 caracteres');
    } else {
        selectedRequest.response = responseText;
        $.ajax({
            type: "POST",
            url: "/requests/update",
            dataType: "json",
            data: {
                request: JSON.stringify(selectedRequest)
            },
            success: function() {
                $.ajax({
                    type: 'POST',
                    url: '/email/send_email/',
                    data: {
                        to: selectedRequest.client_email,
                        subject: 'Resposta ao seu pedido de intervenção Nº: ' + selectedRequest.id,
                        text: 'Resposta ao seu pedido de intervenção para o equipamento.\n\nNúmero de série: ' + selectedRequest.serial_number + '\n\nResposta: ' + responseText
                    }
                });
                showOkResponsePopup('Resposta a pedido de intervenção', 'Resposta enviada com sucesso');
            },
            error: function() {
                showErrorPopup('Resposta a pedido de intervenção', 'Resposta enviada sem sucesso');
            }
        });
    }
}

function showOkResponsePopup(title, messageToShow) {
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: title,
        message: messageToShow,
        buttons: [{
            label: 'Ok',
            action: function(dialogRef) {
                dialogRef.close();
                window.location.href = '/requests/';
            }
        }]
    });
}