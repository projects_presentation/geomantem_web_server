function downloadButtonClick() {
    var listFromTable = $("#reportsGrid").jsGrid("option", "data");
    var filteredDataList = [];
    for (var i = 0; i < listFromTable.length; ++i) {
        filteredDataList.push({
            "Número de série": listFromTable[i].serial_number,
            "Iniciou": moment(listFromTable[i].started).format("YYYY-MM-DD HH:mm:ss"),
            "Terminou": moment(listFromTable[i].ended).format("YYYY-MM-DD HH:mm:ss"),
            "Reparações efectuadas": listFromTable[i].repair_name,
            "Tipo de equipamento / Serviço": listFromTable[i].brand_name,
            "Nome / Marca / Modelo": listFromTable[i].model_name,
            "Estado após reparação": listFromTable[i].status_name
        });
    }
    alasql("SELECT * INTO XLSX('relatorio.xlsx',{headers:true}) FROM ? ", [filteredDataList]);
}