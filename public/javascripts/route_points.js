function fillRoutePointsTable(route_id) {

    $.ajax({
        type: "GET",
        url: "/statuses/get"
    }).done(function(statuses) {
        $.ajax({
            type: "GET",
            url: "/clients/get"
        }).done(function(clients) {
            $.ajax({
                type: "GET",
                url: "/brands/get"
            }).done(function(brands) {
                $.ajax({
                    type: "GET",
                    url: "/models/get"
                }).done(function(models) {

                    jsGrid.fields.myDateFieldStart = getDateField(true);
                    jsGrid.fields.myDateFieldEnd = getDateField(false);

                    statuses.unshift({ id: "0", name: "" });
                    clients.unshift({ id: "0", name: "" });
                    brands.unshift({ id: "0", name: "" });
                    models.unshift({ id: "0", name: "" });

                    $("#routePointsGrid").jsGrid({
                        width: "90%",
                        filtering: false,
                        inserting: false,
                        editing: false,
                        sorting: true,
                        paging: true,
                        autoload: true,
                        pageSize: 10,
                        pageButtonCount: 5,

                        rowClick: function(args) {
                            fillPicturesReport(args.item.route_point_id);
                        },

                        onRefreshed: function(args) {
                            var modelsField = args.grid.fields[3];
                            modelsField.items = models;
                            $(".models-insert-css").empty().append(modelsField.insertTemplate());
                        },


                        controller: {
                            loadData: function(filter) {
                                filter.route_id = route_id
                                return $.ajax({
                                    type: "GET",
                                    url: "/route_points/get_formated",
                                    data: filter
                                });
                            }
                        },

                        fields: getTableFieldsForRoutePoints(statuses, clients, brands, models)
                    });

                });

            });
        });
    });
};

function getTableFieldsForRoutePoints(statuses, clients, brands, models) {
    var fields = [{
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O número de série do equipamento deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        {
            name: "characteristics",
            type: "text",
            title: "Características",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "As características do equipamento não devem conter mais do que 500 caracteres. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length <= 500;
                }
            }
        },
        {
            name: "brand_id",
            type: "select",
            title: "Tipo de equipamento / Serviço",
            items: brands,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo Tipo de equipamento / Serviço não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.brand_id !== 0;
                },
            },
            filterTemplate: function() {
                var modelsField = this._grid.fields[3];
                modelsField.selectedIndex = 0;
                var $filterControl = jsGrid.fields.select.prototype.filterTemplate.call(this);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-filter-css").empty().append(modelsField.filterTemplate());
                });
                return $filterControl;
            },
            insertTemplate: function() {
                var modelsField = this._grid.fields[3];
                modelsField.selectedIndex = 0;
                var $filterControl = jsGrid.fields.select.prototype.insertTemplate.call(this);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-insert-css").empty().append(modelsField.insertTemplate());
                });
                return $filterControl;
            },
            editTemplate: function(item, value) {
                var modelsField = this._grid.fields[3];
                var $filterControl = jsGrid.fields.select.prototype.editTemplate.call(this, item, value);
                $filterControl.on('change', function() {
                    var selectedBrandId = parseInt($(this).val(), 10);
                    if (selectedBrandId === 0) {
                        modelsField.items = models;
                    } else {
                        var brandModels = [];
                        for (var i = 0; i < models.length; ++i) {
                            if (models[i].brand_id === selectedBrandId) {
                                brandModels.push(models[i]);
                            }
                        }
                        brandModels.unshift({ id: "0", name: "" });
                        modelsField.items = brandModels;
                    }
                    $(".models-edit-css").empty().append(modelsField.editTemplate());
                });
                return $filterControl;
            }
        },
        {
            name: "model_id",
            type: "select",
            title: "Nome / Marca / Modelo",
            items: models,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo Nome / Marca / Modelo não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.model_id !== 0;
                },
            },
            filtercss: 'models-filter-css',
            insertcss: 'models-insert-css',
            editcss: 'models-edit-css'
        },
        {
            name: "client_id",
            type: "select",
            title: "Cliente",
            items: clients,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo cliente não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.client_id !== 0;
                },
            },
        },
        {
            name: "status_id",
            type: "select",
            title: "Estado",
            items: statuses,
            valueField: "id",
            valueType: "number",
            textField: "name",
            selectedIndex: 0,
            validate: {
                message: function(value, item) {
                    return "O campo estado não pode ficar em branco";
                },
                validator: function(value, item) {
                    return item.status_id !== 0;
                },
            },
        },
        {
            name: "observations",
            type: "text",
            title: "Observações",
            align: "center",
        },
        { name: "started", type: "myDateFieldStart", title: "Iniciou reparação", align: "center", readOnly: true },
        { name: "ended", type: "myDateFieldEnd", title: "Terminou reparação", align: "center", readOnly: true }
        //{type: "control", editButton: false, deleteButton: false}
    ];

    return fields;
}

function getDateField(start) {
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function(value) {
            var date = moment(value);

            var dateToReturn = date.format("YYYY-MM-DD HH:mm:ss");
            if (dateToReturn.indexOf("Invalid") !== -1) {
                dateToReturn = (start === true ? "Não iniciou reparação" : " Não terminou reparação");
            }
            return dateToReturn;
        },
    });
    return MyDateField;
}

function fillPicturesReport(route_point_id) {
    $.ajax({
        type: 'get',
        url: '/photos/get',
        data: {
            route_point_id: route_point_id,
        },
        success: function(data) {
            var report_images_container = document.getElementById("report_images_container");
            document.getElementById('photos_div').style.display = data.length === 0 ? 'none' : 'block';
            if (data.length === 0) {
                report_images_container.innerHTML = "";
                showOkPopup('Imagens de relatório', 'Não existem imagens para este ponto de reparação');
            } else {
                var html = "";
                $.each(data, function(index, value) {
                    //report_images_container preencher com as imagens para o relatório
                    html += '<img src="/images/' + value.filename + '" class="img-rounded imageFull" alt="' + value.filename + '" width="200" height="300" style="margin: 20px;">'
                });
                report_images_container.innerHTML = html;
                addImageFull();
            }
        }
    });
}

function addImageFull() {
    $('.imageFull').on('click', function() {
        $('.imagepreview').attr('src', $(this).attr('src'));
        $('#imagemodal').modal('show');
    });
}