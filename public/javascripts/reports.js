function fillRoutesTable() {


    jsGrid.fields.myDateFieldStart = getDateField(true);
    jsGrid.fields.myDateFieldEnd = getDateField(false);

    $("#routesGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: false,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,

        deleteConfirm: "Deseja realmente apagar a rota?",

        rowClick: function(args) {
            showRoutePointsReport(args.item);
        },

        controller: {
            loadData: function (filter) {
                return $.ajax({
                    type: "GET",
                    url: "/routes/get",
                    data: filter
                });
            },                           
            deleteItem: function (item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/routes/delete",
                    data: item
                });
            }
        },

        fields: getTableFields()
    });
};

function getTableFields() {
    var fields = [
        {
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center"
        },
        {name: "started", type: "myDateFieldStart", title: "Iniciou reparação", align: "center", readOnly: true},
        {name: "ended", type: "myDateFieldEnd", title: "Terminou reparação", align: "center", readOnly: true},
        {type: "control", editButton: false}
    ];

    return fields;
}

function getDateField(start) {
    var MyDateField = function (config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function (date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function (value) {
            var date = moment(value);

            var dateToReturn = date.format("YYYY-MM-DD HH:mm:ss");
            if (dateToReturn.indexOf("Invalid") !== -1) {
                dateToReturn = (start === true ? "Não iniciou reparação" : " Não terminou reparação");
            }
            return dateToReturn;
        },
    });
    return MyDateField;
}

function showRoutePointsReport(route){
    document.getElementById('route_points_div').style.display = 'block';
    document.getElementById('reports_div').style.display = 'block';
    fillRoutePointsTable(route.id);
    fillReportsTable(route.id);
}

