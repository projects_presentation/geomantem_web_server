function fillLogsTable() {

    jsGrid.fields.myDateField = GetDateField();

    $("#logsGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: false,
        editing: false,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 20,
        pageButtonCount: 5,

        controller: {
            loadData: function (filter) {
                return $.ajax({
                    type: "GET",
                    url: "/logs/get",
                    data: filter
                });
            }
        },

        fields: getTableFields()
    });
};

function getTableFields() {
    var fields = [
        {
            name: "username",
            type: "text",
            title: "Nome de utilizador",
            align: "center"
        },
                {
            name: "email",
            type: "text",
            title: "Email de utilizador",
            align: "center"
        },
                {
            name: "text",
            type: "text",
            title: "Texto",
            align: "center"
        },
        {name: "date", type: "myDateField", title: "Data", width: 150, align: "center", readOnly: true},
        {type: "control", deleteButton: false, editButton:false}
    ];

    return fields;
}

function GetDateField() {
    var MyDateField = function (config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function (date1, date2) {
            return new Date(date1) - new Date(date2);
        },

        itemTemplate: function (value) {
            var date = moment(value);
            return date.format("YYYY-MM-DD HH:mm:ss");
        },
    });
    return MyDateField;
}

