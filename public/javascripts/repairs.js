function fillRepairsTable() {

    $("#repairsGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 300,
        pageButtonCount: 5,

        deleteConfirm: "Deseja realmente apagar o tipo de reparação?",
        invalidMessage: "Dados de preenchimento são inválidos",

        controller: {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "/repairs/get",
                    data: filter
                });
            },
            insertItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "/repairs/insert",
                    data: item
                });
            },
            updateItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "/repairs/update",
                    data: item
                });
            },
            deleteItem: function(item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/repairs/delete",
                    data: item
                });
            }
        },

        fields: getTableFields()
    });
};

function getTableFields() {
    var fields = [{
            name: "description",
            type: "text",
            title: "Descrição",
            align: "center",
            validate: {
                message: function(value, item) {
                    return "O tipo de reparação de equipamento deve conter pelo menos 1 caracter e no máximo 100. O valor: " + (value === "" ? "Vazio" : value) + " não é válido";
                },
                validator: function(value, item) {
                    return value.toString().length > 0 && value.toString().length <= 100;
                }
            }
        },
        { type: "control" }
    ];

    return fields;
}