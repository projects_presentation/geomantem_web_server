function fillTechniciansTable() {


    jsGrid.fields.myDateFieldStart = getDateField(true);
    jsGrid.fields.myDateFieldEnd = getDateField(false);

    $("#routesGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: false,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,

        rowClick: function(args) {
            showRoutePointsReport(args.item);
            $("#routesGrid").jsGrid("refresh");
        },

        onDataLoaded: function(args) {
            if (args.data.length === 0) {
                document.getElementById('route_points_report').style.display = 'none';
            }
        },

        controller: {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "/routes/get_technician",
                    data: filter
                });
            }
        },

        fields: getTableFields()
    });
};

function getTableFields() {
    var fields = [{
            name: "serial_number",
            type: "text",
            title: "Número de série",
            align: "center"
        },
        { name: "started", type: "myDateFieldStart", title: "Iniciou reparação", align: "center", readOnly: true },
        { name: "ended", type: "myDateFieldEnd", title: "Terminou reparação", align: "center", readOnly: true },
        {
            type: "control",
            editButton: false,
            deleteButton: false,

            itemTemplate: function(value, item) {
                var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

                var $customButton = $("<button>")
                    .text(item.started === null ? 'Iniciar' : 'Terminar')
                    .click(function(e) {
                        var jsgrid = $("#routesGrid");
                        if (item.started === null) {
                            startRouteRepairPopup(item, jsgrid);
                        } else if (item.ended === null) {
                            endRouteRepairPopup(item, jsgrid);
                        }
                        e.stopPropagation();
                    });

                return $result.add($customButton);
            }
        }
    ];

    return fields;
}

function getDateField(start) {
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function(value) {
            var date = moment(value);

            var dateToReturn = date.format("YYYY-MM-DD HH:mm:ss");
            if (dateToReturn.indexOf("Invalid") !== -1) {
                dateToReturn = (start === true ? "Não iniciou reparação" : " Não terminou reparação");
            }
            return dateToReturn;
        },
    });
    return MyDateField;
}

function showRoutePointsReport(route) {
    fillRoutePointsTable(route);
}