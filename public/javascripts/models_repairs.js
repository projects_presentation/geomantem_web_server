function fillModelsRepairsTable(model_id) {

    $("#modelsRepairsGrid").jsGrid({
        width: "90%",
        filtering: true,
        inserting: false,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 300,
        pageButtonCount: 5,

        onItemUpdated: function(args) {
            updateModelsRepairsTable(args.grid.data, model_id);
        },

        controller: {
            loadData: function(filter) {
                filter.model_id = model_id;
                return $.ajax({
                    type: "GET",
                    url: "/models_repairs/get",
                    data: filter
                });
            }
        },

        fields: getModelsRepairsTableFields()
    });


};

function getModelsRepairsTableFields() {
    var fields = [{
            name: "description",
            type: "text",
            title: "Descrição",
            align: "center",
            readOnly: true
        },
        { name: "checked", type: "checkbox", title: "Aplicar a Nome / Marca / Modelo", sorting: false },
        {
            type: "control",
            editButton: true,
            deleteButton: false,
            clearFilterButton: false,
            modeSwitchButton: false
        }
    ];

    return fields;
}

function updateModelsRepairsTable(data, model_id) {
    $.ajax({
        type: "POST",
        url: "/models_repairs/update",
        data: {
            list: JSON.stringify(data),
            model_id: model_id
        }
    }).done(function() {

    });
}