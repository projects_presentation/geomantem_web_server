var mysql = require('./connection_manager');

function getClientsList(clientFilter, listOfClients) {
    var query = "SELECT * FROM clients WHERE name LIKE '%" + (clientFilter.name === undefined ? "" : clientFilter.name) + "%'";

    query += " AND address LIKE '%" + (clientFilter.address === undefined ? "" : clientFilter.address) + "%'";

    if (clientFilter.company_id != 0) {
        query += " AND company_id='" + clientFilter.company_id + "'"
    }
    query += ' ORDER BY name ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting clients list: ' + err);
            listOfClients(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfClients(err ? null : rows);
            });
        }
    });
}

function insertClient(client, insertClientResult) {
    var query = "INSERT INTO clients (name,address,company_id) VALUES ('" + client.name + "','" + client.address + "','" + client.company_id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting client into database: ' + err);
            insertClientResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting client into database: ' + err);
                    insertClientResult(null);
                } else {
                    client.company_id = parseInt(client.company_id, 10);
                    client.id = res.insertId;
                    insertClientResult(client);
                }
            });
        }
    });
}

function updateClient(client, updateClientResult) {
    var query = "UPDATE clients SET name='" + client.name + "', address='" + client.address + "' WHERE id = " + client.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating client in database: ' + err);
            updateClientResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateClientResult(null);
                } else {
                    client.company_id = parseInt(client.company_id, 10);
                    updateClientResult(client);
                }
            });
        }
    });
}

function deleteClient(client, deleteClientResult) {
    var query = "DELETE FROM clients WHERE id = " + client.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting client from database: ' + err);
            deleteClientResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteClientResult(err ? null : client);
            });
        }
    });
}

module.exports.getClientsList = getClientsList;
module.exports.insertClient = insertClient;
module.exports.updateClient = updateClient;
module.exports.deleteClient = deleteClient;