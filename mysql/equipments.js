var mysql = require('./connection_manager');
var async = require('async');

function getEquipmentsList(equipmentFilter, listOfEquipments) {
    var query = "SELECT * FROM equipments WHERE serial_number LIKE '%" + (equipmentFilter.serial_number === undefined ? "" : equipmentFilter.serial_number) + "%'";
    query += " AND characteristics LIKE '%" + (equipmentFilter.characteristics === undefined ? "" : equipmentFilter.characteristics) + "%'";
    query += " AND observations LIKE '%" + (equipmentFilter.observations === undefined ? "" : equipmentFilter.observations) + "%'";

    if (equipmentFilter.brand_id != 0) {
        query += " AND brand_id='" + equipmentFilter.brand_id + "'"
    }
    if (equipmentFilter.model_id != 0) {
        query += " AND model_id='" + equipmentFilter.model_id + "'"
    }
    if (equipmentFilter.company_id != 0) {
        query += " AND company_id='" + equipmentFilter.company_id + "'"
    }
    if (equipmentFilter.client_id != 0) {
        query += " AND client_id='" + equipmentFilter.client_id + "'"
    }
    if (equipmentFilter.status_id != 0) {
        query += " AND status_id='" + equipmentFilter.status_id + "'"
    }
    if (equipmentFilter.warehouse_id != -1) {
        query += " AND warehouse_id='" + equipmentFilter.warehouse_id + "'"
    }
    query += ' ORDER BY serial_number ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipments list: ' + err);
            listOfEquipments(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfEquipments(err ? null : rows);
            });
        }
    });
}

function getEquipmentsFormatedList(equipmentFilter, listOfEquipments) {
    var query = "SELECT * FROM get_equipments WHERE serial_number LIKE '%" + (equipmentFilter.serial_number === undefined ? "" : equipmentFilter.serial_number) + "%'";
    query += " AND characteristics LIKE '%" + (equipmentFilter.characteristics === undefined ? "" : equipmentFilter.characteristics) + "%'";
    query += " AND observations LIKE '%" + (equipmentFilter.observations === undefined ? "" : equipmentFilter.observations) + "%'";
    query += " AND address LIKE '%" + (equipmentFilter.address === undefined ? "" : equipmentFilter.address) + "%'";
    if (equipmentFilter.brand_id != 0) {
        query += " AND brand_id='" + equipmentFilter.brand_id + "'"
    }
    if (equipmentFilter.model_id != 0) {
        query += " AND model_id='" + equipmentFilter.model_id + "'"
    }
    if (equipmentFilter.company_id != 0) {
        query += " AND company_id='" + equipmentFilter.company_id + "'"
    }
    if (equipmentFilter.client_id != 0) {
        query += " AND client_id='" + equipmentFilter.client_id + "'"
    }
    if (equipmentFilter.status_id != 0) {
        query += " AND status_id='" + equipmentFilter.status_id + "'"
    }
    if (equipmentFilter.warehouse_id != -1 && equipmentFilter.warehouse_id != undefined) {
        query += " AND warehouse_id='" + equipmentFilter.warehouse_id + "'"
    }
    query += ' ORDER BY serial_number ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipments list: ' + err);
            listOfEquipments(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                //listOfEquipments(err ? null : rows);
                // Alterado para devolver as images de reparação por cada um dos equipamentos
                async.eachSeries(rows, function(item, callback) {
                    getEquipmentPicturesForMap(item.id, function(photosForMap) {
                        item.photos_for_map = photosForMap;
                        if (photosForMap.length != 0) {
                            item.repair_observations = photosForMap[0].observations;
                        }
                        getEquipmentRepairNamesForMap(item.id, function(repairsNames) {
                            item.repairs_names = repairsNames;
                            callback();
                        })

                    });
                }, function() {
                    listOfEquipments(err ? null : rows);
                });
            });
        }
    });
}

function getEquipmentsListInWarehouse(equipmentFilter, listOfEquipments) {
    var query = "SELECT id,serial_number FROM equipments WHERE company_id='" + equipmentFilter.company_id + "' AND warehouse_id='" + equipmentFilter.warehouse_id + "' ORDER BY serial_number ASC";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipments in warehouse list: ' + err);
            listOfEquipments(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfEquipments(err ? null : rows);
            });
        }
    });
}

function getEquipmentPicturesForMap(equipmentID, photosForMap) {
    var query = "SELECT route_point_photos.`route_point_id`,route_point_photos.`filename`,route_points.`equipment_id`,route_points.`observations` FROM route_point_photos JOIN route_points ON route_points.`id` = route_point_photos.`route_point_id` WHERE equipment_id = '" + equipmentID + "' ORDER BY filename DESC LIMIT 8";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipments photo for map: ' + err);
            photosForMap(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                photosForMap(err ? null : rows);
            });
        }
    });
}

function getEquipmentRepairNamesForMap(equipmentID, repairNames) {
    var query = "SELECT started,repair_name FROM get_reports WHERE equipment_id = '" + equipmentID + "' ORDER BY started LIMIT 10";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipments repair names for map: ' + err);
            repairNames(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                repairNames(err ? null : rows);
            });
        }
    });
}

function getEquipmentLocation(equipmentAID, equipmentLocation) {
    var query = "SELECT latitude,longitude,address,serial_number FROM equipments WHERE id =" + equipmentAID + " LIMIT 1";;

    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipment location: ' + err);
            equipmentLocation(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                equipmentLocation(err ? null : rows);
            });
        }
    });
}

function insertEquipment(equipment, insertEquipmentResult) {
    var query = "INSERT INTO equipments (serial_number,characteristics,observations,brand_id,model_id,latitude,longitude,company_id,client_id,status_id,photo_url,replaced_serial_number) VALUES ('" + equipment.serial_number + "','" + equipment.characteristics + "','" + equipment.observations + "','" + equipment.brand_id + "','" + equipment.model_id + "','38.931128','-9.247341','" + equipment.company_id + "','" + equipment.client_id + "','" + equipment.status_id + "','no_image.png','" + equipment.replaced_serial_number + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting equipment into database: ' + err);
            insertEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting equipment into database: ' + err);
                    insertEquipmentResult(null);
                } else {
                    equipment.brand_id = parseInt(equipment.brand_id, 10);
                    equipment.model_id = parseInt(equipment.model_id, 10);
                    equipment.company_id = parseInt(equipment.company_id, 10);
                    equipment.client_id = parseInt(equipment.client_id, 10);
                    equipment.status_id = parseInt(equipment.status_id, 10);
                    equipment.warehouse_id = parseInt(equipment.warehouse_id, 10);
                    equipment.id = res.insertId;
                    insertEquipmentResult(equipment);
                }
            });
        }
    });
}

function updateEquipment(equipment, updateEquipmentResult) {
    var query = "UPDATE equipments SET serial_number='" + equipment.serial_number + "',characteristics='" + equipment.characteristics + "',observations = '" + equipment.observations + "',brand_id = '" + equipment.brand_id + "',model_id ='" + equipment.model_id + "', latitude='" + equipment.latitude + "',longitude='" + equipment.longitude + "', client_id='" + equipment.client_id + "', status_id='" + equipment.status_id + "', warehouse_id='" + equipment.warehouse_id + "', address = '" + equipment.address + "', replaced_serial_number='" + equipment.replaced_serial_number + "' WHERE id = " + equipment.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating equipment in database: ' + err);
            updateEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateEquipmentResult(null);
                } else {
                    equipment.brand_id = parseInt(equipment.brand_id, 10);
                    equipment.model_id = parseInt(equipment.model_id, 10);
                    equipment.company_id = parseInt(equipment.company_id, 10);
                    equipment.client_id = parseInt(equipment.client_id, 10);
                    equipment.status_id = parseInt(equipment.status_id, 10);
                    equipment.warehouse_id = parseInt(equipment.warehouse_id, 10);
                    equipment.id = res.insertId;
                    updateEquipmentResult(equipment);
                }
            });
        }
    });
}

function updateEquipmentStatus(equipmentId, statusID, updateEquipmentResult) {
    var query = "UPDATE equipments SET status_id='" + statusID + "' WHERE id = " + equipmentId + " LIMIT 1";;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating equipment in database: ' + err);
            updateEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateEquipmentResult(null);
                } else {
                    updateEquipmentResult(true);
                }
            });
        }
    });
}

function updateEquipmentWarehouse(equipmentId, warehouseID, updateEquipmentResult) {
    var query = "UPDATE equipments SET warehouse_id='" + warehouseID + "' WHERE id = " + equipmentId + " LIMIT 1";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating equipment warehouse in database: ' + err);
            updateEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateEquipmentResult(null);
                } else {
                    updateEquipmentResult(true);
                }
            });
        }
    });
}

function updateEquipmentLocation(equipmentID, newLatitude, newLongitude, newAddress, replaced_serial_number, updateEquipmentResult) {
    var query = "UPDATE equipments SET latitude='" + newLatitude + "', longitude='" + newLongitude + "', address='" + newAddress + "', replaced_serial_number = '" + replaced_serial_number + "' WHERE id ='" + equipmentID + "'";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating equipment location in database: ' + err);
            updateEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateEquipmentResult(null);
                } else {
                    updateEquipmentResult(true);
                }
            });
        }
    });
}

function deleteEquipment(equipment, deleteEquipmentResult) {
    var query = "DELETE FROM equipments WHERE id = " + equipment.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting equipment from database: ' + err);
            deleteEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteEquipmentResult(err ? null : equipment);
            });
        }
    });
}

module.exports.getEquipmentsList = getEquipmentsList;
module.exports.getEquipmentsFormatedList = getEquipmentsFormatedList;
module.exports.getEquipmentsListInWarehouse = getEquipmentsListInWarehouse;
module.exports.getEquipmentLocation = getEquipmentLocation;
module.exports.insertEquipment = insertEquipment;
module.exports.updateEquipment = updateEquipment;
module.exports.updateEquipmentStatus = updateEquipmentStatus;
module.exports.updateEquipmentWarehouse = updateEquipmentWarehouse;
module.exports.updateEquipmentLocation = updateEquipmentLocation;
module.exports.deleteEquipment = deleteEquipment;