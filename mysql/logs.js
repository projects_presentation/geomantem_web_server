var mysql = require('./connection_manager');

function getLogsList(logFilter, listOfLogs) {
    var query = "SELECT * FROM get_logs WHERE email LIKE '%" + (logFilter.email === undefined ? "" : logFilter.email) + "%'";
    if (logFilter.company_id != 0) {
        query += " AND company_id='" + logFilter.company_id + "'"
    }
    query += ' ORDER BY date DESC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting logs list: ' + err);
            listOfLogs(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfLogs(err ? null : rows);
            });
        }
    });
}

function insertLog(log) {
    var query = "INSERT INTO logs SET ? ";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting log into database: ' + err);
        } else {
            connection.query(query, log, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting log into database: ' + err);
                }
            });
        }
    });
}

module.exports.getLogsList = getLogsList;
module.exports.insertLog = insertLog;