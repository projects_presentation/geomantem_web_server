var mysql = require('./connection_manager');

function getStatusesList(listOfStatuses) {
    var query = "SELECT * FROM eq_status";
    query += ' ORDER BY id ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting statuses list: ' + err);
            listOfStatuses(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfStatuses(err ? null : rows);
            });
        }
    });
}

module.exports.getStatusesList = getStatusesList;