var mysql = require('./connection_manager');

function getRepairsDecriptionsList(modelRepairFilter, listOfRepairs) {
    var query = "SELECT * FROM eq_repairs"
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting models repairs list: ' + err);
            listOfRepairs(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRepairs(err ? null : rows);
            });
        }
    });
}

function getModelsRepairsList(modelRepairFilter, listOfRepairsForModel) {
    var query = "SELECT * FROM eq_models_repairs WHERE model_id='" + modelRepairFilter.model_id + "'"
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting models repairs list: ' + err);
            listOfRepairsForModel(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRepairsForModel(err ? null : rows);
            });
        }
    });
}

function insertRoutePointRepair(routePointRepair, insertRoutePointRepairResult) {
    var query = "INSERT INTO route_points_repairs (route_point_id,repair_id) VALUES ('" + routePointRepair.route_point_id + "','" + routePointRepair.repair_id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting route point repair into database: ' + err);
            insertRoutePointRepairResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting route point repair into database: ' + err);
                    insertRoutePointRepairResult(null);
                } else {
                    insertRoutePointRepairResult(routePointRepair);
                }
            });
        }
    });
}

function deleteRoutePointRepair(routePointRepair, deleteRoutePointRepairResult) {
    var query = "DELETE FROM route_points_repairs WHERE route_point_id = " + routePointRepair.route_point_id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting route point repair from database: ' + err);
            deleteRoutePointRepairResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteRoutePointRepairResult(err ? null : routePointRepair);
            });
        }
    });
}

module.exports.getRepairsDecriptionsList = getRepairsDecriptionsList;
module.exports.getModelsRepairsList = getModelsRepairsList;
module.exports.insertRoutePointRepair = insertRoutePointRepair;
module.exports.deleteRoutePointRepair = deleteRoutePointRepair;