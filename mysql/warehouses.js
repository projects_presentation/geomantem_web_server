var mysql = require('./connection_manager');

function getWarehousesList(filter, listToReturn) {
    var query = "SELECT * FROM warehouses WHERE name LIKE '%" + (filter.name === undefined ? "" : filter.name) + "%'";
    if (filter.client_id != 0) {
        query += " AND client_id='" + filter.client_id + "'"
    }
    if (filter.company_id != 0) {
        query += " AND company_id='" + filter.company_id + "'"
    }
    query += ' ORDER BY name ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting warehouses list: ' + err);
            listToReturn(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listToReturn(err ? null : rows);
            });
        }
    });
}

function getWarehousesListWithNoCompany(filter, listToReturn) {
    var query = "SELECT * FROM warehouses WHERE name LIKE '%" + (filter.name === undefined ? "" : filter.name) + "%'";
    if (filter.client_id != 0 && filter.client_id != undefined) {
        query += " AND client_id='" + filter.client_id + "'"
    }
    if (filter.company_id != 0) {
        query += " AND company_id='" + filter.company_id + "' OR company_id = 0"
    }
    query += ' ORDER BY name ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting warehouses list: ' + err);
            listToReturn(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listToReturn(err ? null : rows);
            });
        }
    });
}

function getAvailableWarehousesList(clientID, companyID, listToReturn) {
    var query = "SELECT * FROM warehouses WHERE client_id = '" + clientID + "' AND company_id = '" + companyID + "' ORDER BY name ASC";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting available warehouses list: ' + err);
            listToReturn(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listToReturn(err ? null : rows);
            });
        }
    });
}

function insertWarehouse(warehouse, result) {
    var query = "INSERT INTO warehouses SET ?";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting warehouse into database: ' + err);
            result(null);
        } else {
            connection.query(query, warehouse, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting warehouse into database: ' + err);
                    result(null);
                } else {
                    warehouse.id = res.insertId;
                    warehouse.client_id = parseInt(warehouse.client_id, 10);
                    warehouse.company_id = parseInt(warehouse.company_id, 10);
                    result(warehouse);
                }
            });
        }
    });
}

function updateWarehouse(warehouse, result) {
    var query = "UPDATE warehouses SET name='" + warehouse.name + "', latitude='" + warehouse.latitude + "', longitude='" + warehouse.longitude + "', client_id='" + warehouse.client_id + "' WHERE id = " + warehouse.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating warehouse in database: ' + err);
            result(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error updating warehouse in database: ' + err);
                    result(null);
                } else {
                    warehouse.client_id = parseInt(warehouse.client_id, 10);
                    warehouse.company_id = parseInt(warehouse.company_id, 10);
                    result(warehouse);
                }
            });
        }
    });
}

function deleteWarehouse(warehouse, result) {

    // Marcar todos os equipamentos com 0 para apontar para o "Fora do estaleiro"

    var query = "DELETE FROM warehouses WHERE id = " + warehouse.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting warehouse from database: ' + err);
            result(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                result(err ? null : warehouse);
            });
        }
    });
}

module.exports.getWarehousesList = getWarehousesList;
module.exports.getWarehousesListWithNoCompany = getWarehousesListWithNoCompany;
module.exports.getAvailableWarehousesList = getAvailableWarehousesList;
module.exports.insertWarehouse = insertWarehouse;
module.exports.updateWarehouse = updateWarehouse;
module.exports.deleteWarehouse = deleteWarehouse;