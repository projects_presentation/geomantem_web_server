var mysql = require('./connection_manager');

function getEquipmentsList(equipmentFilter, listOfEquipments) {
    var query = "SELECT * FROM equipments WHERE serial_number LIKE '%" + (equipmentFilter.serial_number === undefined ? "" : equipmentFilter.serial_number) + "%'";
    query += " AND characteristics LIKE '%" + (equipmentFilter.characteristics === undefined ? "" : equipmentFilter.characteristics) + "%'";

    if (equipmentFilter.brand_id != 0) {
        query += " AND brand_id='" + equipmentFilter.brand_id + "'"
    }
    if (equipmentFilter.model_id != 0) {
        query += " AND model_id='" + equipmentFilter.model_id + "'"
    }
    if (equipmentFilter.company_id != 0) {
        query += " AND company_id='" + equipmentFilter.company_id + "'"
    }
    if (equipmentFilter.client_id != 0) {
        query += " AND client_id='" + equipmentFilter.client_id + "'"
    }
    if (equipmentFilter.status_id != 0) {
        query += " AND status_id='" + equipmentFilter.status_id + "'"
    }
    query += ' ORDER BY serial_number ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipments list: ' + err);
            listOfEquipments(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfEquipments(err ? null : rows);
            });
        }
    });
}

function getEquipmentsFormatedList(equipmentFilter, listOfEquipments) {
    var query = "SELECT * FROM get_equipments WHERE serial_number LIKE '%" + (equipmentFilter.serial_number === undefined ? "" : equipmentFilter.serial_number) + "%'";
    query += " AND characteristics LIKE '%" + (equipmentFilter.characteristics === undefined ? "" : equipmentFilter.characteristics) + "%'";
    query += " AND address LIKE '%" + (equipmentFilter.address === undefined ? "" : equipmentFilter.address) + "%'";
    if (equipmentFilter.brand_id != 0) {
        query += " AND brand_id='" + equipmentFilter.brand_id + "'"
    }
    if (equipmentFilter.model_id != 0) {
        query += " AND model_id='" + equipmentFilter.model_id + "'"
    }
    if (equipmentFilter.company_id != 0) {
        query += " AND company_id='" + equipmentFilter.company_id + "'"
    }
    if (equipmentFilter.client_id != 0) {
        query += " AND client_id='" + equipmentFilter.client_id + "'"
    }
    if (equipmentFilter.status_id != 0) {
        query += " AND status_id='" + equipmentFilter.status_id + "'"
    }
    query += ' ORDER BY serial_number ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipments list: ' + err);
            listOfEquipments(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfEquipments(err ? null : rows);
            });
        }
    });
}


function insertEquipment(equipment, insertEquipmentResult) {
    var query = "INSERT INTO equipments (serial_number,characteristics,brand_id,model_id,latitude,longitude,company_id,client_id,status_id) VALUES ('" + equipment.serial_number + "','" + equipment.characteristics + "','" + equipment.brand_id + "','" + equipment.model_id + "','38.931128','-9.247341','" + equipment.company_id + "','" + equipment.client_id + "','" + equipment.status_id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting equipment into database: ' + err);
            insertEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting equipment into database: ' + err);
                    insertEquipmentResult(null);
                } else {
                    equipment.brand_id = parseInt(equipment.brand_id, 10);
                    equipment.model_id = parseInt(equipment.model_id, 10);
                    equipment.company_id = parseInt(equipment.company_id, 10);
                    equipment.client_id = parseInt(equipment.client_id, 10);
                    equipment.status_id = parseInt(equipment.status_id, 10);
                    equipment.id = res.insertId;
                    insertEquipmentResult(equipment);
                }
            });
        }
    });
}

function updateEquipment(equipment, updateEquipmentResult) {
    var query = "UPDATE equipments SET serial_number='" + equipment.serial_number + "',characteristics='" + equipment.characteristics + "',brand_id = '" + equipment.brand_id + "',model_id ='" + equipment.model_id + "', latitude='" + equipment.latitude + "',longitude='" + equipment.longitude + "', client_id='" + equipment.client_id + "', status_id='" + equipment.status_id + "' WHERE id = " + equipment.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating equipment in database: ' + err);
            updateEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateEquipmentResult(null);
                } else {
                    equipment.brand_id = parseInt(equipment.brand_id, 10);
                    equipment.model_id = parseInt(equipment.model_id, 10);
                    equipment.company_id = parseInt(equipment.company_id, 10);
                    equipment.client_id = parseInt(equipment.client_id, 10);
                    equipment.status_id = parseInt(equipment.status_id, 10);
                    equipment.id = res.insertId;
                    updateEquipmentResult(equipment);
                }
            });
        }
    });
}

function deleteEquipment(equipment, deleteEquipmentResult) {
    var query = "DELETE FROM equipments WHERE id = " + equipment.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting equipment from database: ' + err);
            deleteEquipmentResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteEquipmentResult(err ? null : equipment);
            });
        }
    });
}

module.exports.getEquipmentsList = getEquipmentsList;
module.exports.getEquipmentsFormatedList = getEquipmentsFormatedList;
module.exports.insertEquipment = insertEquipment;
module.exports.updateEquipment = updateEquipment;
module.exports.deleteEquipment = deleteEquipment;