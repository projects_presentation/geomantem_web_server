var mysql = require('./connection_manager');

function getModelsList(modelFilter, listOfModels) {
    var query = "SELECT * FROM eq_models WHERE name LIKE '%" + (modelFilter.name === undefined ? "" : modelFilter.name) + "%'";
    if (modelFilter.brand_id != 0 && modelFilter.brand_id !== undefined) {
        query += " AND brand_id='" + modelFilter.brand_id + "'"
    }
    if (modelFilter.company_id != 0) {
        query += " AND company_id='" + modelFilter.company_id + "'"
    }
    query += ' ORDER BY name ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting models list: ' + err);
            listOfModels(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfModels(err ? null : rows);
            });
        }
    });
}


function insertModel(model, insertModelResult) {
    var query = "INSERT INTO eq_models (name,icon,brand_id,company_id) VALUES ('" + model.name + "','" + model.icon + "','" + model.brand_id + "','" + model.company_id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting model into database: ' + err);
            insertModelResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting model into database: ' + err);
                    insertModelResult(null);
                } else {
                    model.brand_id = parseInt(model.brand_id, 10);
                    model.id = res.insertId;
                    insertModelResult(model);
                }
            });
        }
    });
}

function updateModel(model, updateModelResult) {
    var query = "UPDATE eq_models SET name='" + model.name + "',icon='" + model.icon + "',brand_id = '" + model.brand_id + "' WHERE id = " + model.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating model in database: ' + err);
            updateModelResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateModelResult(null);
                } else {
                    model.brand_id = parseInt(model.brand_id, 10);
                    updateModelResult(model);
                }
            });
        }
    });
}

function deleteModel(model, deleteModelResult) {
    var query = "DELETE FROM eq_models WHERE id = " + model.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting model from database: ' + err);
            deleteModelResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteModelResult(err ? null : model);
            });
        }
    });
}

module.exports.getModelsList = getModelsList;
module.exports.insertModel = insertModel;
module.exports.updateModel = updateModel;
module.exports.deleteModel = deleteModel;