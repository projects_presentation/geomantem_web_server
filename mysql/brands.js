var mysql = require('./connection_manager');

function getBrandsList(brandFilter, listOfBrands) {
    var query = "SELECT * FROM eq_brands WHERE name LIKE '%" + (brandFilter.name === undefined ? "" : brandFilter.name) + "%'";
    if (brandFilter.company_id != 0) {
        query += " AND company_id='" + brandFilter.company_id + "'"
    }
    query += ' ORDER BY name ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting brands list: ' + err);
            listOfBrands(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfBrands(err ? null : rows);
            });
        }
    });
}

function insertBrand(brand, insertBrandResult) {
    var query = "INSERT INTO eq_brands (name,company_id) VALUES ('" + brand.name + "','" + brand.company_id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting brand into database: ' + err);
            insertBrandResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting brand into database: ' + err);
                    insertBrandResult(null);
                } else {
                    brand.id = res.insertId;
                    brand.company_id = parseInt(brand.company_id, 10);
                    insertBrandResult(brand);
                }
            });
        }
    });
}

function updateBrand(brand, updateBrandResult) {
    var query = "UPDATE eq_brands SET name='" + brand.name + "' WHERE id = " + brand.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating brand in database: ' + err);
            updateBrandResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error updating brand in database: ' + err);
                    updateBrandResult(null);
                } else {
                    brand.company_id = parseInt(brand.company_id, 10);
                    updateBrandResult(brand);
                }
            });
        }
    });
}

function deleteBrand(brand, deleteBrandResult) {
    var query = "DELETE FROM eq_brands WHERE id = " + brand.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting brand from database: ' + err);
            deleteBrandResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteBrandResult(err ? null : brand);
            });
        }
    });
}

module.exports.getBrandsList = getBrandsList;
module.exports.insertBrand = insertBrand;
module.exports.updateBrand = updateBrand;
module.exports.deleteBrand = deleteBrand;