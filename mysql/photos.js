var mysql = require('./connection_manager');

function getPhotosList(photoFilter, listOfPhotos) {
    var query = "SELECT * FROM route_point_photos WHERE route_point_id = '" + photoFilter.route_point_id + "'";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting photos list: ' + err);
            listOfPhotos(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfPhotos(err ? null : rows);
            });
        }
    });
}

function insertPhoto(photo, insertPhotoResult) {
    var query = "INSERT INTO route_point_photos SET ?";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting photo into database: ' + err);
            insertPhotoResult(null);
        } else {
            connection.query(query, photo, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting photo into database: ' + err);
                    insertPhotoResult(null);
                } else {
                    insertPhotoResult(photo);
                }
            });
        }
    });
}

module.exports.getPhotosList = getPhotosList;
module.exports.insertPhoto = insertPhoto;