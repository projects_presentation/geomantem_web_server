var mysql = require('./connection_manager');

var dateFormat = require('dateFormat');

function getRequestsList(requestFilter, listOfRequests) {
    var query = "SELECT * FROM get_requests WHERE serial_number LIKE '%" + (requestFilter.serial_number === undefined ? "" : requestFilter.serial_number) + "%'";
    query += " AND id LIKE '%" + (requestFilter.id === undefined ? "" : requestFilter.id) + "%'";
    query += " AND text LIKE '%" + (requestFilter.text === undefined ? "" : requestFilter.text) + "%'";
    query += " AND client_name LIKE '%" + (requestFilter.client_name === undefined ? "" : requestFilter.client_name) + "%'";
    query += " AND response LIKE '%" + (requestFilter.response === undefined ? "" : requestFilter.response) + "%'";

    if (requestFilter.equipment_id != 0 && requestFilter.equipment_id !== undefined) {
        query += " AND equipment_id='" + requestFilter.brand_id + "'"
    }
    if (requestFilter.company_id != 0) {
        query += " AND company_id='" + requestFilter.company_id + "'"
    }
    query += ' ORDER BY started ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting requests list: ' + err);
            listOfRequests(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRequests(err ? null : rows);
            });
        }
    });
}

function updateRequest(request, updateRequestResult) {

    var query = "UPDATE requests SET response='" + request.response + "',ended='" + dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss") + "' WHERE id = " + request.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating request in database: ' + err);
            updateRequestResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateRequestResult(null);
                } else {
                    request.brand_id = parseInt(request.brand_id, 10);
                    updateRequestResult(request);
                }
            });
        }
    });
}

function deleteRequest(request, deleteRequestResult) {
    var query = "DELETE FROM requests WHERE id = " + request.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting request from database: ' + err);
            deleteRequestResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteRequestResult(err ? null : request);
            });
        }
    });
}

module.exports.getRequestsList = getRequestsList;
module.exports.updateRequest = updateRequest;
module.exports.deleteRequest = deleteRequest;