var mysql = require('./connection_manager');

function getReportsList(reportFilter, listOfReports) {
    var query = "SELECT * FROM get_reports WHERE route_id ='" + reportFilter.route_id + "'";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting reports list: ' + err);
            listOfReports(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfReports(err ? null : rows);
            });
        }
    });
}

module.exports.getReportsList = getReportsList;