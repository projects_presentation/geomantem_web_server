var mysql = require('./connection_manager');

function getRepairsList(repairFilter, listOfRepairs) {
    var query = "SELECT * FROM eq_repairs WHERE description LIKE '%" + (repairFilter.description === undefined ? "" : repairFilter.description) + "%'";
    if (repairFilter.company_id != 0) {
        query += " AND company_id='" + repairFilter.company_id + "'"
    }
    query += ' ORDER BY description ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting repairs list: ' + err);
            listOfRepairs(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRepairs(err ? null : rows);
            });
        }
    });
}

function insertRepair(repair, insertRepairResult) {
    var query = "INSERT INTO eq_repairs (description,company_id) VALUES ('" + repair.description + "','" + repair.company_id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting repair into database: ' + err);
            insertRepairResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting repair into database: ' + err);
                    insertRepairResult(null);
                } else {
                    repair.id = res.insertId;
                    repair.company_id = parseInt(repair.company_id, 10);
                    insertRepairResult(repair);
                }
            });
        }
    });
}

function updateRepair(repair, updateRepairResult) {
    var query = "UPDATE eq_repairs SET description='" + repair.description + "' WHERE id = " + repair.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating repair in database: ' + err);
            updateRepairResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error updating repair in database: ' + err);
                    updateRepairResult(null);
                } else {
                    repair.company_id = parseInt(repair.company_id, 10);
                    updateRepairResult(repair);
                }
            });
        }
    });
}

function deleteRepair(repair, deleteRepairResult) {
    var query = "DELETE FROM eq_repairs WHERE id = " + repair.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting repair from database: ' + err);
            deleteRepairResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteRepairResult(err ? null : repair);
            });
        }
    });
}

module.exports.getRepairsList = getRepairsList;
module.exports.insertRepair = insertRepair;
module.exports.updateRepair = updateRepair;
module.exports.deleteRepair = deleteRepair;