var mysql = require('mysql');
var mysqlSettings;

var poolCreated = false;
var pool;

var settingsForDevelopment = {
    connectionLimit: 15,
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'geo_assistance'
};

var settingsForProduction = {
    connectionLimit: 15,
    host: 'localhost',
    user: 'geo_assistance',
    password: 'p@22w0rd',
    database: 'geo_assistance'
};

if (process.env.NODE_ENV === 'development') {
    console.log('App is running on development environment');
    mysqlSettings = settingsForDevelopment;
} else {
    console.log('App is running on production environment');
    mysqlSettings = settingsForProduction;
}

function connection() {
    return mysql.createConnection(mysqlSettings);
}

function getPool() {
    if (poolCreated) {
        return pool;
    } else {
        poolCreated = true;
        pool = mysql.createPool(mysqlSettings);
        return pool;
    }
}

module.exports.mysqlSettings = mysqlSettings;
module.exports.connection = connection;
module.exports.getPool = getPool;