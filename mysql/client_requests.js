var mysql = require('./connection_manager');

var dateFormat = require('dateFormat');

function getRequestsList(requestFilter, listOfRequests) {
    var query = "SELECT * FROM get_requests WHERE serial_number LIKE '%" + (requestFilter.serial_number === undefined ? "" : requestFilter.serial_number) + "%'";
    query += " AND id LIKE '%" + (requestFilter.id === undefined ? "" : requestFilter.id) + "%'";
    query += " AND text LIKE '%" + (requestFilter.text === undefined ? "" : requestFilter.text) + "%'";
    query += " AND client_id = '" + requestFilter.client_id + "'";
    query += " AND response LIKE '%" + (requestFilter.response === undefined ? "" : requestFilter.response) + "%'";
    query += ' ORDER BY started ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting requests list: ' + err);
            listOfRequests(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRequests(err ? null : rows);
            });
        }
    });
}

function getEquipmentsList(equipmentFilter, listOfEquipments) {
    var query = "SELECT * FROM equipments WHERE client_id='" + equipmentFilter.client_id + "'";
    query += ' ORDER BY serial_number ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting equipments list: ' + err);
            listOfEquipments(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfEquipments(err ? null : rows);
            });
        }
    });
}

function insertRequest(request, insertRequestResult) {

    var query = "INSERT INTO requests SET ?";
    var pool = mysql.getPool();

    var started = dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
    request.started = started;

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting request from database: ' + err);
            insertRequestResult(null);
        } else {
            connection.query(query, request, function(err, res) {
                connection.release();
                insertRequestResult(err ? null : request);
            });
        }
    });
}

function deleteRequest(request, deleteRequestResult) {
    var query = "DELETE FROM requests WHERE id = " + request.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting request from database: ' + err);
            deleteRequestResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteRequestResult(err ? null : request);
            });
        }
    });
}

module.exports.getRequestsList = getRequestsList;
module.exports.getEquipmentsList = getEquipmentsList;
module.exports.insertRequest = insertRequest;
module.exports.deleteRequest = deleteRequest;