var mysql = require('./connection_manager');
var crypto = require("../utilities/password");

function updateAccountPassword(account, updateAccountResult) {

    var query = "UPDATE accounts SET username='" + account.username + "',password = '" + account.password + "', salt='" + account.salt + "' WHERE id = '" + account.id + "' LIMIT 1";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error in MySQL while updating password: ' + err);
            updateAccountResult(false);
        } else {
            connection.query(query, function(err, result) {
                connection.release();
                if (err) console.log('Error in MySQL while updating password: ' + err);
                updateAccountResult(err ? false : true);
            });
        }
    });
}

function getAccountByEmail(email, getAccountResult) {
    var query = "SELECT * FROM accounts WHERE email = '" + email + "' LIMIT 1";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error in MySQL getting account by email: ' + err);
            getAccountResult(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                getAccountResult(err ? null : rows);
            });
        }
    });
}

function accountExists(email, queryResult) {
    var query = "SELECT id,salt FROM accounts WHERE email = '" + email + "' LIMIT 1";

    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error in MySQL while checking for account existence: ' + err);
            queryResult(false);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                if (err) {
                    console.log('Error in MySQL while checking for account existence: ' + err);
                    queryResult({
                        accountExists: false
                    });
                } else {
                    queryResult({
                        accountExists: rows.length > 0,
                        salt: rows.length > 0 ? rows[0].salt : ""
                    });
                }
            });
        }
    });
}

function createAccount(userToCreate, queryResult) {
    var query = "INSERT INTO accounts SET ? ";

    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error in MySQL while inserting new account: ' + err);
            queryResult(false);
        } else {
            connection.query(query, userToCreate, function(err, result) {
                connection.release();
                if (err) {
                    console.log('Error in MySQL while inserting new account: ' + err);
                    queryResult(false);
                } else {
                    queryResult(result.affectedRows != 0);
                }
            });
        }
    });
}


function validateAccount(email, password, result) {

    accountExists(email, function(accountExistsResult) {
        // User exists lets validate the username and password
        if (accountExistsResult.accountExists == true) {
            var hashedPassword = crypto.getHashedPassword(password, accountExistsResult.salt);
            var query = "SELECT * FROM accounts WHERE email = '" + email + "' AND password = '" + hashedPassword + "' LIMIT 1";

            var pool = mysql.getPool();
            pool.getConnection(function(err, connection) {
                if (err) {
                    console.log('Error in MySQL while validating account: ' + err);
                    result(null);
                } else {
                    connection.query(query, function(err, rows) {
                        connection.release();
                        if (err) {
                            console.log('Error in MySQL while validating account: ' + err);
                            result(null);
                        } else {
                            // User exists and a valid password was provided
                            result(rows.length > 0 ? rows[0] : null);
                        }
                    });
                }
            });
        }
        // User doesnt exist!
        else {
            result(null);
        }
    });
};

module.exports.updateAccountPassword = updateAccountPassword;
module.exports.getAccountByEmail = getAccountByEmail;
module.exports.accountExists = accountExists;
module.exports.createAccount = createAccount;
module.exports.validateAccount = validateAccount;