var mysql = require('./connection_manager');
var accountClass = require('../classes/accounts');

function getClientsAccountsList(accountFilter, listOfAccounts) {
    var query = "SELECT * FROM accounts WHERE username LIKE '%" + (accountFilter.username === undefined ? "" : accountFilter.username) + "%'";
    query += " AND email LIKE '%" + (accountFilter.email === undefined ? "" : accountFilter.email) + "%'";

    if (accountFilter.client_id != 0) {
        query += " AND client_id='" + accountFilter.client_id + "'"
    }

    if (accountFilter.company_id != 0) {
        query += " AND company_id='" + accountFilter.company_id + "'"
    }
    query += "AND client = '1' ORDER BY username ASC";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting clients accounts list: ' + err);
            listOfAccounts(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfAccounts(err ? null : rows);
            });
        }
    });
}

function getTechniciansAccountsList(accountFilter, listOfAccounts) {
    var query = "SELECT * FROM accounts WHERE username LIKE '%" + (accountFilter.username === undefined ? "" : accountFilter.username) + "%'";
    query += " AND email LIKE '%" + (accountFilter.email === undefined ? "" : accountFilter.email) + "%'";

    if (accountFilter.company_id != 0) {
        query += " AND company_id='" + accountFilter.company_id + "'"
    }
    query += "AND technician = '1' ORDER BY username ASC";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting technicians accounts list: ' + err);
            listOfAccounts(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfAccounts(err ? null : rows);
            });
        }
    });
}

function insertClientAccount(clientAccount, insertAccountResult) {

    var accountToInsert = new accountClass.Account(clientAccount.username, clientAccount.email, '12345');
    accountToInsert.manager = 0;
    accountToInsert.client = 1;
    accountToInsert.technician = 0;
    accountToInsert.company_id = clientAccount.company_id;
    accountToInsert.client_id = clientAccount.client_id;
    accountToInsert.authorize_repairs = clientAccount.authorize_repairs === true ? 1 : 0;

    var query = "INSERT INTO accounts SET ? ";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting account into database: ' + err);
            insertAccountResult(null);
        } else {
            connection.query(query, accountToInsert, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting account into database: ' + err);
                    insertAccountResult(null);
                } else {
                    clientAccount.id = res.insertId;
                    clientAccount.manager = parseInt(clientAccount.manager, 10);
                    clientAccount.client = parseInt(clientAccount.client, 10);
                    clientAccount.technician = parseInt(clientAccount.technician, 10);
                    clientAccount.company_id = parseInt(clientAccount.company_id, 10);
                    clientAccount.client_id = parseInt(clientAccount.client_id, 10);
                    insertAccountResult(clientAccount);
                }
            });
        }
    });
}

function insertTechnicianAccount(technicianAccount, insertAccountResult) {

    var accountToInsert = new accountClass.Account(technicianAccount.username, technicianAccount.email, '12345');
    accountToInsert.manager = 0;
    accountToInsert.client = 0;
    accountToInsert.technician = 1;
    accountToInsert.company_id = technicianAccount.company_id;
    accountToInsert.client_id = 0;

    var query = "INSERT INTO accounts SET ? ";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting technician account into database: ' + err);
            insertAccountResult(null);
        } else {
            connection.query(query, accountToInsert, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting technician account into database: ' + err);
                    insertAccountResult(null);
                } else {
                    technicianAccount.id = res.insertId;
                    technicianAccount.manager = parseInt(technicianAccount.manager, 10);
                    technicianAccount.client = parseInt(technicianAccount.client, 10);
                    technicianAccount.technician = parseInt(technicianAccount.technician, 10);
                    technicianAccount.company_id = parseInt(technicianAccount.company_id, 10);
                    technicianAccount.client_id = parseInt(technicianAccount.client_id, 10);
                    insertAccountResult(technicianAccount);
                }
            });
        }
    });
}

function updateAccount(account, updateAccountResult) {
    var query = "UPDATE accounts SET username='" + account.username + "', email = '" + account.email + "', client_id='" + account.client_id + "', authorize_repairs='" + (account.authorize_repairs === 'true' ? 1 : 0) + "' WHERE id = " + account.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating account in database: ' + err);
            updateAccountResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log(err);
                    updateAccountResult(null);
                } else {
                    account.manager = parseInt(account.manager, 10);
                    account.company_id = parseInt(account.company_id, 10);
                    account.client_id = parseInt(account.client_id, 10);
                    //account.authorize_repairs = account.authorize_repairs === 1 ? true : false;
                    updateAccountResult(account);
                }
            });
        }
    });
}

function deleteAccount(account, deleteAccountResult) {
    var query = "DELETE FROM accounts WHERE id = " + account.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting account from database: ' + err);
            deleteAccountResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteAccountResult(err ? null : account);
            });
        }
    });
}

module.exports.getClientsAccountsList = getClientsAccountsList;
module.exports.getTechniciansAccountsList = getTechniciansAccountsList;
module.exports.insertClientAccount = insertClientAccount;
module.exports.insertTechnicianAccount = insertTechnicianAccount;
module.exports.updateAccount = updateAccount;
module.exports.deleteAccount = deleteAccount;