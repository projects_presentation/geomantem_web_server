var mysql = require('./connection_manager');
var dateFormat = require('dateformat');

function getSerialNumber(serialNumberResult) {
    var query = "SELECT UUID() as uuid";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting route serial number: ' + err);
            serialNumberResult(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                if (err) {
                    console.log('Error getting route serial number: ' + err);
                    serialNumberResult(null);
                } else {
                    serialNumberResult(rows[0]);
                }
            });
        }
    });
}

function getRoutes(routeFilter, listOfRoutes) {
    var query = "SELECT * FROM routes WHERE serial_number LIKE '%" + (routeFilter.serial_number === undefined ? "" : routeFilter.serial_number) + "%'";
    if (routeFilter.company_id != 0) {
        query += " AND company_id='" + routeFilter.company_id + "'"
    }
    query += ' ORDER BY serial_number ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting routes for report list: ' + err);
            listOfRoutes(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRoutes(err ? null : rows);
            });
        }
    });
}

function getRoutesForTechnician(routeFilter, listOfRoutes) {
    var query = "SELECT * FROM routes WHERE serial_number LIKE '%" + (routeFilter.serial_number === undefined ? "" : routeFilter.serial_number) + "%'";
    if (routeFilter.company_id != 0) {
        query += " AND company_id='" + routeFilter.company_id + "'"
    }
    query += 'AND ended IS NULL ORDER BY serial_number ASC';
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting routes for report list: ' + err);
            listOfRoutes(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRoutes(err ? null : rows);
            });
        }
    });
}

function insertRoute(route, insertRouteResult) {

    var query = "INSERT INTO routes (serial_number,company_id) VALUES ('" + route.routeName + "','" + route.company_id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting route into database: ' + err);
            insertRouteResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting route into database: ' + err);
                    insertRouteResult(null);
                } else {
                    route.id = res.insertId;
                    route.company_id = parseInt(route.company_id, 10);
                    route.serial_number = route.routeName;
                    insertRouteResult(true);
                }
            });
        }
    });
}

function updateStarted(route, updateStartedResult) {
    var started = dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
    var query = "UPDATE routes SET started='" + started + "' WHERE id = " + route.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating route in database: ' + err);
            updateStartedResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error updating route in database: ' + err);
                    updateStartedResult(null);
                } else {
                    updateStartedResult(route);
                }
            });
        }
    });
}

function updateEnded(route, updateStartedResult) {
    var started = dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
    var query = "UPDATE routes SET ended='" + started + "' WHERE id = " + route.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating route in database: ' + err);
            updateStartedResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error updating route in database: ' + err);
                    updateStartedResult(null);
                } else {
                    updateStartedResult(route);
                }
            });
        }
    });
}

function deleteRoute(route, deleteRouteResult) {
    var query = "DELETE FROM routes WHERE id = " + route.id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting route from database: ' + err);
            deleteRouteResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteRouteResult(err ? null : route);
            });
        }
    });
}

module.exports.getRoutes = getRoutes;
module.exports.getRoutesForTechnician = getRoutesForTechnician;
module.exports.insertRoute = insertRoute;
module.exports.updateStarted = updateStarted;
module.exports.updateEnded = updateEnded;
module.exports.deleteRoute = deleteRoute;