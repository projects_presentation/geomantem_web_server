var mysql = require('./connection_manager');

function getModelsRepairsList(modelRepairFilter, listOfRepairs) {
    var query = "SELECT * FROM eq_models_repairs WHERE model_id='" + modelRepairFilter.model_id + "'"
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting models repairs list: ' + err);
            listOfRepairs(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRepairs(err ? null : rows);
            });
        }
    });
}

function insertModelRepair(modelRepair, insertModelRepairResult) {
    var query = "INSERT INTO eq_models_repairs (model_id,repair_id) VALUES ('" + modelRepair.model_id + "','" + modelRepair.id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting model repair into database: ' + err);
            insertModelRepairResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting model repair into database: ' + err);
                    insertModelRepairResult(null);
                } else {
                    modelRepair.id = res.insertId;
                    modelRepair.model_id = parseInt(modelRepair.model_id, 10);
                    modelRepair.repair_id = parseInt(modelRepair.repair_id, 10);
                    insertModelRepairResult(modelRepair);
                }
            });
        }
    });
}

function deleteModelRepair(modelRepair, deleteModelRepairResult) {
    var query = "DELETE FROM eq_models_repairs WHERE model_id = " + modelRepair.model_id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error deleting models repair from database: ' + err);
            deleteModelRepairResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                deleteModelRepairResult(err ? null : modelRepair);
            });
        }
    });
}

module.exports.getModelsRepairsList = getModelsRepairsList;
module.exports.insertModelRepair = insertModelRepair;
module.exports.deleteModelRepair = deleteModelRepair;