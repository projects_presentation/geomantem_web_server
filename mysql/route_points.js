var mysql = require('./connection_manager');
var dateFormat = require('dateformat');

function getRoutePointsList(route_id, listOfRoutePoints) {
    var query = "SELECT * FROM get_route_points WHERE route_id = '" + route_id + "'";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting route points list: ' + err);
            listOfRoutePoints(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                listOfRoutePoints(err ? null : rows);
            });
        }
    });
}

function getRoutePoint(route_point_id, routePoint) {
    var query = "SELECT * FROM get_route_points WHERE route_point_id = '" + route_point_id + "' LIMIT 1";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error getting route point: ' + err);
            routePoint(null);
        } else {
            connection.query(query, function(err, rows) {
                connection.release();
                routePoint(err ? null : rows[0]);
            });
        }
    });
}

function insertRoutePoint(route_id, equipment_id, insertRoutePointResult) {
    var query = "INSERT INTO route_points (route_id,equipment_id) VALUES ('" + route_id + "','" + equipment_id + "')";
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error inserting route point into database: ' + err);
            insertRoutePointResult(false);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error inserting route point into database: ' + err);
                }
                insertRoutePointResult(err ? false : true);
            });
        }
    });
}

function updateObservations(routePoint, updateObservationsResult) {
    var query = "UPDATE route_points SET observations='" + routePoint.observations + "' WHERE id = " + routePoint.route_point_id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating route point observations in database: ' + err);
            updateObservationsResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error updating route point observations in database: ' + err);
                    updateObservationsResult(null);
                } else {
                    updateObservationsResult(routePoint);
                }
            });
        }
    });
}

function updateStarted(routePoint, updateStartedResult) {
    var started = dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
    var query = "UPDATE route_points SET started='" + started + "' WHERE id = " + routePoint.route_point_id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating route point in database: ' + err);
            updateStartedResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error updating route point in database: ' + err);
                    updateStartedResult(null);
                } else {
                    updateStartedResult(routePoint);
                }
            });
        }
    });
}

function updateEnded(routePoint, updateStartedResult) {
    var started = dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
    var query = "UPDATE route_points SET ended='" + started + "' WHERE id = " + routePoint.route_point_id;
    var pool = mysql.getPool();

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error updating route point in database: ' + err);
            updateStartedResult(null);
        } else {
            connection.query(query, function(err, res) {
                connection.release();
                if (err) {
                    console.log('Error updating route point in database: ' + err);
                    updateStartedResult(null);
                } else {
                    updateStartedResult(routePoint);
                }
            });
        }
    });
}


module.exports.getRoutePointsList = getRoutePointsList;
module.exports.getRoutePoint = getRoutePoint;
module.exports.insertRoutePoint = insertRoutePoint;
module.exports.updateObservations = updateObservations;
module.exports.updateStarted = updateStarted;
module.exports.updateEnded = updateEnded;