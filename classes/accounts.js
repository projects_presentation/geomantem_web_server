var passwordGeneration = require("../utilities/password");

function Account(username, email, password) {

    var passwordData = passwordGeneration.generateHashedPassword(password);
    this.username = username;
    this.email = email;
    this.password = passwordData.password;
    this.salt = passwordData.salt;
};

module.exports.Account = Account;