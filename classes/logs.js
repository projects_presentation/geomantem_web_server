var dateFormat = require('dateformat');

function Log(account_id, text_id, company_id) {

    this.account_id = account_id;
    this.text_id = text_id;
    this.company_id = company_id;
    this.date = dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
};

module.exports.Log = Log;