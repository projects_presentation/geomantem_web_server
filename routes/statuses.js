var express = require('express');
var router = express.Router();

var statusesDB = require('../mysql/statuses');

router.get('/get', function(req, res) {
    if (req.session.account) {
        statusesDB.getStatusesList(function(listOfStatuses) {
            res.status('200').send(listOfStatuses);
        });
    } else res.status('500').send(null);
});

module.exports = router;