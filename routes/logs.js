var express = require('express');
var router = express.Router();

var logsDB = require('../mysql/logs');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('logs', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.manager === 1) {
        req.query.company_id = req.session.account.company_id;
        logsDB.getLogsList(req.query, function(listOfBrands) {
            res.status('200').send(listOfBrands);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/insert', function(req, res) {
    if (req.session.account) {
        req.body.company_id = req.session.account.company_id;
        logsDB.insertBrand(req.body, function(insertBrandResult) {
            res.status('200').send(insertBrandResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;