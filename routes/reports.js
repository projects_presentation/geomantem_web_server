var express = require('express');
var router = express.Router();

var reportsDB = require('../mysql/reports');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('reports', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.manager === 1) {
        reportsDB.getReportsList(req.query, function(listOfReports) {
            res.status('200').send(listOfReports);
        });
    } else {
        res.status('500').send(null);
    }
});


module.exports = router;