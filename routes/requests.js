var express = require('express');
var router = express.Router();

var async = require('async');
var dateFormat = require('dateformat');

var multer = require('multer');
var fs = require('fs-extra');

var requestsDB = require('../mysql/requests');

var path = require('path');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('requests', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/add_photo_to_request', function(req, res) {
    res.status(200).sendFile(path.join(__dirname + '/../views/add_photo_to_request.html'));
});

router.get('/get', function(req, res) {
    if (req.session.account) {
        req.query.company_id = req.session.account.company_id;
        requestsDB.getRequestsList(req.query, function(listOfRequests) {
            res.status('200').send(listOfRequests);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.manager === 1) {
        requestsDB.updateRequest(JSON.parse(req.body.request), function(updateRequestResult) {
            res.status('200').send(updateRequestResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager === 1) {
        requestsDB.deleteRequest(req.body, function(deleteModelResult) {
            res.status('200').send(deleteModelResult);
        });
    } else {
        res.status('500').send(null);
    }
});

/* Upload photo to request */
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, __dirname + 'images/')
    },
    filename: function(req, file, cb) {
        var filename = dateFormat('yyyymmddHHMMssl') + '.jpg'
        cb(null, filename);
    }
})

var upload = multer({
    storage: storage
});

router.post('/upload_photo', upload.single("uploadedfile"), function(req, res) {
    var file = req.file;
    responseData = {
        success: false
    };
    fs.copySync('routesimages/' + file.filename, './public/images/' + file.filename);
    var photoToInsert = {
        route_point_id: req.body.route_point_id,
        filename: file.filename
    }
    responseData.success = true;
    responseData.filename = file.filename;
    res.status('200').send(responseData);
});

module.exports = router;