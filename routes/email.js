var nodemailer = require('nodemailer');

var express = require('express');
var router = express.Router();

let transporter = nodemailer.createTransport({
    host: 'smtp-pt.securemail.pro',
    port: 465,
    secure: true,
    auth: {
        user: 'admin@geomantem.pt',
        pass: 'G30Em@il'
    }
});

router.post('/send_email', function(req, res) {
    var emailData = req.body;
    if (req.session.account.manager === 1 || req.session.account.client === 1) {
        let mailOptions = {
            from: '"Geral GeoMantem" <admin@geomantem.pt>', // sender address
            to: emailData.to, // list of receivers
            subject: emailData.subject, // Subject line
            text: emailData.text
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                res.status('500').send(false);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
            res.status('200').send(true);
        });
    } else {
        res.status('500').send(false);
    }
});

module.exports = router;