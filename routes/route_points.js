var express = require('express');
var router = express.Router();

var routePointsDB = require('../mysql/route_points');
var equipmentsDB = require('../mysql/equipments');

router.get('/get_formated', function(req, res) {
    if (req.session.account.manager === 1 || req.session.account.technician === 1) {
        routePointsDB.getRoutePointsList(req.query.route_id, function(listOfRoutePoints) {
            res.status('200').send(listOfRoutePoints);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update_started', function(req, res) {
    if (req.session.account.technician === 1) {
        routePointsDB.updateStarted(JSON.parse(req.body.routePoint), function(updateResult) {
            res.status('200').send(updateResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update_ended', function(req, res) {
    if (req.session.account.technician === 1) {
        var routePoint = JSON.parse(req.body.routePoint);
        equipmentsDB.updateEquipmentStatus(routePoint.equipment_id, routePoint.status_id, function(updateEquipmentResult) {
            routePointsDB.updateEnded(routePoint, function(updateResult) {
                routePointsDB.updateObservations(routePoint, function(updateObservationsResult) {
                    res.status('200').send(updateObservationsResult);
                });

            });
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;