var express = require('express');
var router = express.Router();

var photosDB = require('../mysql/photos');


router.get('/get', function(req, res) {
    if (req.session.account.manager === 1) {
        photosDB.getPhotosList(req.query, function(listOfPhotos) {
            res.status('200').send(listOfPhotos);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;