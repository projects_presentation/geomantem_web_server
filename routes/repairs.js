var express = require('express');
var router = express.Router();

var repairsDB = require('../mysql/repairs');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('repairs', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.manager === 1) {
        req.query.company_id = req.session.account.company_id;
        repairsDB.getRepairsList(req.query, function(listOfRepairs) {
            res.status('200').send(listOfRepairs);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/insert', function(req, res) {
    if (req.session.account.manager === 1) {
        req.body.company_id = req.session.account.company_id;
        repairsDB.insertRepair(req.body, function(insertRepairResult) {
            res.status('200').send(insertRepairResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.manager === 1) {
        repairsDB.updateRepair(req.body, function(updateRepairResult) {
            res.status('200').send(updateRepairResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager === 1) {
        repairsDB.deleteRepair(req.body, function(deleteRepairResult) {
            res.status('200').send(deleteRepairResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;