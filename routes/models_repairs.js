var express = require('express');
var router = express.Router();
var async = require('async');

var repairsDB = require('../mysql/repairs');
var modelsRepairsDB = require('../mysql/models_repairs');

router.get('/get', function(req, res) {
    if (req.session.account.manager === 1 || req.session.account.technician === 1) {
        req.query.company_id = req.session.account.company_id;
        repairsDB.getRepairsList(req.query, function(listOfRepairs) {
            modelsRepairsDB.getModelsRepairsList(req.query, function(listOfModelsRepairs) {
                if (Array.isArray(listOfRepairs)) {
                    for (var i = 0; i < listOfRepairs.length; ++i) {
                        if (Array.isArray(listOfModelsRepairs)) {
                            for (var j = 0; j < listOfModelsRepairs.length; ++j) {
                                if (listOfModelsRepairs[j].repair_id === listOfRepairs[i].id) {
                                    listOfRepairs[i].checked = true;
                                    break;
                                } else listOfRepairs[i].checked = false;
                            }
                        } else {
                            listOfRepairs[i].checked = listOfModelsRepairs.repair_id === listOfRepairs[i].id ? true : false;
                            break;
                        }
                    }
                } else {
                    if (Array.isArray(listOfModelsRepairs)) {
                        for (var j = 0; j < listOfModelsRepairs.length; ++j) {
                            if (listOfModelsRepairs[j].repair_id === listOfRepairs.id) {
                                listOfRepairs.checked = true;
                                break;
                            } else listOfRepairs.checked = false;
                        }
                    } else {
                        listOfRepairs.checked = listOfModelsRepairs.repair_id === listOfRepairs.id ? true : false;
                    }
                }
                res.status('200').send(listOfRepairs);
            });

        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.manager === 1) {
        modelsRepairsDB.deleteModelRepair(req.body, function(updateRepairResult) {
            var listOfModelsRepairs = JSON.parse(req.body.list);
            async.eachSeries(listOfModelsRepairs, function(item, callback) {
                item.model_id = req.body.model_id;
                if (item.checked === true) {
                    modelsRepairsDB.insertModelRepair(item, function(insertModelRepairResult) {
                        callback();
                    });
                } else {
                    callback();
                }

            }, function() {
                res.status('200').send(updateRepairResult);
            });
        });
    } else {
        res.status('500').send(null);
    }
});


module.exports = router;