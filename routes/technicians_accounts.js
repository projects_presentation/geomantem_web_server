var express = require('express');
var router = express.Router();

var accountsManagementDB = require('../mysql/accounts_management');
var accountsDB = require('../mysql/accounts');
var accountClass = require('../classes/accounts');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('technicians_accounts', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/settings', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('account_management', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.manager === 1) {
        req.query.company_id = req.session.account.company_id;
        accountsManagementDB.getTechniciansAccountsList(req.query, function(listOfAccounts) {
            res.status('200').send(listOfAccounts);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/insert', function(req, res) {
    if (req.session.account.manager === 1) {
        req.body.company_id = req.session.account.company_id;
        accountsManagementDB.insertTechnicianAccount(req.body, function(insertAccountResult) {
            res.status('200').send(insertAccountResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account) {
        accountsManagementDB.updateAccount(req.body, function(updateAccountResult) {
            res.status('200').send(updateAccountResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update_password', function(req, res) {
    if (req.session.account) {
        var accountToUpdate = new accountClass.Account(req.body.name, req.session.account.email, req.body.password);
        accountToUpdate.id = req.session.account.id;
        accountsDB.updateAccountPassword(accountToUpdate, function(updateAccountResult) {
            res.status('200').send(updateAccountResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager === 1) {
        accountsManagementDB.deleteAccount(req.body, function(deleteAccountResult) {
            res.status('200').send(deleteAccountResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;