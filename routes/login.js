var express = require('express');
var router = express.Router();
var accounts = require('../classes/accounts');
var accountsDB = require("../mysql/accounts");

var logClass = require('../classes/logs');
var logsDB = require('../mysql/logs');

router.get("/", function(req, res) {
    var pageToRender = '';
    if (req.session.account) {
        if (req.session.account.manager === 1) pageToRender = 'manager';
        else if (req.session.account.client === 1) pageToRender = 'client';
        else if (req.session.account.technician === 1) pageToRender = 'technician';
        res.render(pageToRender, {
            account: req.session.account
        });
    } else {
        res.render('login', {
            errors: null
        });
    }
});

router.post('/', function(req, res) {

    var email = req.query.email;
    var password = req.query.password;

    req.session.errors = null;
    req.check('email', 'Formato de email inválido').isEmail();
    req.check('password', 'Password inválida').isLength({ min: 1 });

    var errors = req.validationErrors();

    if (errors) {
        res.render("login", {
            errors: errors
        });
    } else {
        accountsDB.validateAccount(req.body.email, req.body.password, function(result) {
            if (result != null) {
                logsDB.insertLog(new logClass.Log(result.id, 1, result.company_id));
                req.session.account = result;
                res.redirect('/');
            } else {
                var errorsList = [];
                errorsList[0] = { param: "name", msg: 'Email e/ou palavra-passe inválido(a)(s)', value: req.body.email };
                res.render("login", {
                    errors: errorsList
                });
            }
        });
    }
});

router.get('/logout', function(req, res) {
    if (req.session.account !== null && req.session.account != undefined) {
        logsDB.insertLog(new logClass.Log(req.session.account.id, 2, req.session.account.company_id));
        req.session.destroy();
    }
    res.redirect('/');

});

module.exports = router;