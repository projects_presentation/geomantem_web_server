var express = require('express');
var router = express.Router();

var brandsDB = require('../mysql/brands');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('brands', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account) {
        req.query.company_id = req.session.account.company_id;
        brandsDB.getBrandsList(req.query, function(listOfBrands) {
            res.status('200').send(listOfBrands);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/insert', function(req, res) {
    if (req.session.account.manager === 1) {
        req.body.company_id = req.session.account.company_id;
        brandsDB.insertBrand(req.body, function(insertBrandResult) {
            res.status('200').send(insertBrandResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.manager === 1) {
        brandsDB.updateBrand(req.body, function(updateBrandResult) {
            res.status('200').send(updateBrandResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager === 1) {
        brandsDB.deleteBrand(req.body, function(deleteBrandResult) {
            res.status('200').send(deleteBrandResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;