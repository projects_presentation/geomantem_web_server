var express = require('express');
var router = express.Router();
var async = require('async');

var equipmentsDB = require('../mysql/equipments');
var routesDB = require('../mysql/routes');
var routePointsDB = require('../mysql/route_points');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('routes', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.manager === 1 || req.session.account.technician === 1) {
        req.query.company_id = req.session.account.company_id;
        routesDB.getRoutes(req.query, function(listOfReports) {
            res.status('200').send(listOfReports);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get_technician', function(req, res) {
    if (req.session.account.technician === 1) {
        req.query.company_id = req.session.account.company_id;
        routesDB.getRoutesForTechnician(req.query, function(listOfReports) {
            res.status('200').send(listOfReports);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get_formated', function(req, res) {
    if (req.session.account.manager === 1) {
        req.query.company_id = req.session.account.company_id;
        equipmentsDB.getEquipmentsFormatedList(req.query, function(listOfEquipments) {
            res.status('200').send(listOfEquipments);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/insert', function(req, res) {
    if (req.session.account.manager === 1) {
        req.body.company_id = req.session.account.company_id;
        routesDB.insertRoute(req.body, function(insertRouteResult) {
            var routePoints = JSON.parse(req.body.routePoints);
            async.eachSeries(routePoints, function(item, callback) {
                routePointsDB.insertRoutePoint(req.body.id, item.id, function(insertRoutePointResult) {
                    callback();
                });
            }, function() {
                res.status('200').send({
                    insertRouteResult
                });
            });

        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update_started', function(req, res) {
    if (req.session.account.technician === 1) {
        var route = JSON.parse(req.body.route);
        routesDB.updateStarted(route, function(updateResult) {
            res.status('200').send(updateResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update_ended', function(req, res) {
    if (req.session.account.technician === 1) {
        var route = JSON.parse(req.body.route);
        routesDB.updateEnded(route, function(updateResult) {
            res.status('200').send(updateResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager) {
        routesDB.deleteRoute(req.body, function(deleteRouteResult) {
            res.status('200').send(deleteRouteResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;