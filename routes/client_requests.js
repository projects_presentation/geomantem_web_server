var express = require('express');
var router = express.Router();

var clientRequestsDB = require('../mysql/client_requests');
var equipmentsDB = require('../mysql/equipments');

router.get('/', function(req, res) {
    if (req.session.account.client === 1) {
        res.render('client_requests', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.client === 1) {
        req.query.client_id = req.session.account.client_id;
        clientRequestsDB.getRequestsList(req.query, function(listOfRequests) {
            res.status('200').send(listOfRequests);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get_equipments', function(req, res) {
    if (req.session.account.client === 1) {
        req.query.client_id = req.session.account.client_id;
        clientRequestsDB.getEquipmentsList(req.query, function(listOfEquipments) {
            res.status('200').send(listOfEquipments);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/insert', function(req, res) {
    if (req.session.account.client === 1) {
        req.body.company_id = req.session.account.company_id;
        req.body.client_id = req.session.account.client_id;
        req.body.client_email = req.session.account.email;
        clientRequestsDB.insertRequest(req.body, function(updateRequestResult) {
            equipmentsDB.updateEquipmentStatus(req.body.equipment_id, 5, function(result) {
                res.status('200').send(true);
            });
        });
    } else {
        res.status('500').send(false);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.client === 1) {
        clientRequestsDB.deleteRequest(req.body, function(deleteModelResult) {
            res.status('200').send(deleteModelResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;