var express = require('express');
var router = express.Router();

var modelsDB = require('../mysql/models');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('models', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account) {
        req.query.company_id = req.session.account.company_id;
        modelsDB.getModelsList(req.query, function(listOfModels) {
            res.status('200').send(listOfModels);
        });
    } else {
        res.status('500').send(null);
    }
});


router.post('/insert', function(req, res) {
    if (req.session.account.manager === 1) {
        req.body.company_id = req.session.account.company_id;
        modelsDB.insertModel(req.body, function(insertModelResult) {
            res.status('200').send(insertModelResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.manager === 1) {
        modelsDB.updateModel(req.body, function(updateModelResult) {
            res.status('200').send(updateModelResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager === 1) {
        modelsDB.deleteModel(req.body, function(deleteModelResult) {
            res.status('200').send(deleteModelResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;