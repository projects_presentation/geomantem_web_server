var express = require('express');
var router = express.Router();

var equipmentsDB = require('../mysql/equipments');

router.get('/', function(req, res) {
    if (req.session.account.client === 1) {
        res.render('client', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.client === 1) {
        req.query.client_id = req.session.account.client_id;
        req.query.company_id = req.session.account.company_id;
        equipmentsDB.getEquipmentsList(req.query, function(listOfEquipments) {
            res.status('200').send(listOfEquipments);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get_formated', function(req, res) {
    if (req.session.account.client === 1) {
        req.query.client_id = req.session.account.client_id;
        req.query.company_id = req.session.account.company_id;
        equipmentsDB.getEquipmentsFormatedList(req.query, function(listOfEquipments) {
            res.status('200').send(listOfEquipments);
        });
    } else {
        res.status('500').send(null);
    }
});


router.post('/insert', function(req, res) {
    if (req.session.account.client === 1) {
        req.query.client_id = req.session.account.client_id;
        req.query.company_id = req.session.account.company_id;
        equipmentsDB.insertEquipment(req.body, function(insertEquipmentResult) {
            res.status('200').send(insertEquipmentResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.client === 1) {
        equipmentsDB.updateEquipment(req.body, function(updateEquipmentResult) {
            res.status('200').send(updateEquipmentResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.client === 1) {
        equipmentsDB.deleteEquipment(req.body, function(deleteEquipmentResult) {
            res.status('200').send(deleteEquipmentResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;