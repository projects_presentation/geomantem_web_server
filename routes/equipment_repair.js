var express = require('express');
var router = express.Router();
var async = require('async');
var dateFormat = require('dateformat');

var multer = require('multer');
var fs = require('fs-extra');

var routePointsDB = require('../mysql/route_points');
var equipmentRepairDB = require('../mysql/equipment_repair');
var photosDB = require('../mysql/photos');

router.get('/', function(req, res) {
    if (req.session.account.technician === 1) {
        routePointsDB.getRoutePoint(req.query.route_point_id, function(route_point) {
            res.render('equipment_repair', {
                account: req.session.account,
                route_point: route_point
            });
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.technician === 1) {
        equipmentRepairDB.getModelsRepairsList(req.query, function(listOfModelsRepairs) {
            res.status('200').send(listOfModelsRepairs);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get_repairs_list', function(req, res) {
    if (req.session.account.technician === 1) {
        equipmentRepairDB.getRepairsDecriptionsList(req.query, function(listOfRepairs) {
            res.status('200').send(listOfRepairs);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.technician === 1) {
        equipmentRepairDB.deleteRoutePointRepair(req.body, function(updateRepairResult) {
            var listOfModelsRepairs = JSON.parse(req.body.list);
            async.eachSeries(listOfModelsRepairs, function(item, callback) {
                item.route_point_id = req.body.route_point_id;
                if (item.checked === true) {
                    equipmentRepairDB.insertRoutePointRepair(item, function(insertModelRepairResult) {
                        callback();
                    });
                } else {
                    callback();
                }

            }, function() {
                res.status('200').send(updateRepairResult);
            });
        });

    } else {
        res.status('500').send(null);
    }
});

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, __dirname + 'images/')
    },
    filename: function(req, file, cb) {
        var filename = dateFormat('yyyymmddHHMMssl') + '.jpg'
        cb(null, filename);
    }
})

var upload = multer({
    storage: storage
});

router.post('/upload_photo', upload.single("uploadedfile"), function(req, res) {
    var file = req.file;
    responseData = {
        success: false
    };
    fs.copySync('routesimages/' + file.filename, './public/images/' + file.filename);
    var photoToInsert = {
        route_point_id: req.body.route_point_id,
        filename: file.filename
    }
    photosDB.insertPhoto(photoToInsert, function(insertPhotoResult) {

    });
    responseData.success = true;
    res.status('200').send(responseData);
});

module.exports = router;