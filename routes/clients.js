var express = require('express');
var router = express.Router();

var clientsDB = require('../mysql/clients');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('clients', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.manager === 1 || req.session.account.technician === 1) {
        req.query.company_id = req.session.account.company_id;
        clientsDB.getClientsList(req.query, function(listOfClients) {
            res.status('200').send(listOfClients);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/insert', function(req, res) {
    if (req.session.account.manager === 1) {
        req.body.company_id = req.session.account.company_id;
        clientsDB.insertClient(req.body, function(insertClientsResult) {
            res.status('200').send(insertClientsResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.manager === 1) {
        clientsDB.updateClient(req.body, function(updateClientResult) {
            res.status('200').send(updateClientResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager === 1) {
        clientsDB.deleteClient(req.body, function(deleteClientResult) {
            res.status('200').send(deleteClientResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;