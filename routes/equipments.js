var express = require('express');
var router = express.Router();

var equipmentsDB = require('../mysql/equipments');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('equipments', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account.manager === 1) {
        req.query.company_id = req.session.account.company_id;
        equipmentsDB.getEquipmentsList(req.query, function(listOfEquipments) {
            res.status('200').send(listOfEquipments);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get_formated', function(req, res) {
    if (req.session.account.manager === 1 || req.session.account.client === 1) {
        req.query.company_id = req.session.account.company_id;
        if (req.session.account.client === 1) {
            req.query.client_id = req.session.account.client_id;
        }
        equipmentsDB.getEquipmentsFormatedList(req.query, function(listOfEquipments) {
            res.status('200').send(listOfEquipments);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get_all_in_warehouse', function(req, res) {
    if (req.session.account) {
        req.query.company_id = req.session.account.company_id;
        equipmentsDB.getEquipmentsListInWarehouse(req.query, function(listOfEquipments) {
            res.status('200').send(listOfEquipments);
        });
    } else {
        res.status('500').send(null);
    }
});


router.post('/insert', function(req, res) {
    if (req.session.account.manager === 1 || req.session.account.client === 1) {
        req.body.company_id = req.session.account.company_id;
        if (req.session.account.client === 1) {
            req.body.client_id = req.session.account.client_id;
        }
        equipmentsDB.insertEquipment(req.body, function(insertEquipmentResult) {
            res.status('200').send(insertEquipmentResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.manager === 1 || req.session.account.client === 1) {
        if (req.session.account.client === 1) {
            req.query.client_id = req.session.account.client_id;
        }
        equipmentsDB.updateEquipment(req.body, function(updateEquipmentResult) {
            res.status('200').send(updateEquipmentResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update_status', function(req, res) {
    if (req.session.account) {
        equipmentsDB.updateEquipmentStatus(req.body.equipmentID, req.body.newStatus, function(updateEquipmentResult) {
            res.status('200').send(updateEquipmentResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update_warehouse', function(req, res) {
    if (req.session.account) {
        equipmentsDB.updateEquipmentWarehouse(req.body.equipmentID, req.body.newWarehouse, function(updateEquipmentResult) {
            res.status('200').send(updateEquipmentResult);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/replace', function(req, res) {
    if (req.session.account) {
        //req.body.equipmentAID; req.body.equipmentBID; req.body.selectedWarehouse;
        equipmentsDB.updateEquipmentWarehouse(req.body.equipmentAID, req.body.selectedWarehouse, function(updateResult) {
            equipmentsDB.updateEquipmentWarehouse(req.body.equipmentBID, 0, function(updateResult) {
                equipmentsDB.getEquipmentLocation(req.body.equipmentAID, function(equipmentLocation) {
                    equipmentsDB.updateEquipmentLocation(req.body.equipmentBID, equipmentLocation[0].latitude, equipmentLocation[0].longitude, equipmentLocation[0].address, equipmentLocation[0].serial_number, function(updateLocationResult) {
                        res.status('200').send(updateLocationResult);
                    });
                });
            });
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager === 1 || req.session.account.client === 1) {
        equipmentsDB.deleteEquipment(req.body, function(deleteEquipmentResult) {
            res.status('200').send(deleteEquipmentResult);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;