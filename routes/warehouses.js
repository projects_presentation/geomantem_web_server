var express = require('express');
var router = express.Router();

var warehousesDB = require('../mysql/warehouses');

router.get('/', function(req, res) {
    if (req.session.account.manager === 1) {
        res.render('warehouses', {
            account: req.session.account
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get', function(req, res) {
    if (req.session.account) {
        req.query.company_id = req.session.account.company_id;
        if (req.session.account.client === 1) {
            req.query.client_id = req.session.account.client_id;
        }
        warehousesDB.getWarehousesList(req.query, function(list) {
            res.status('200').send(list);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/getall', function(req, res) {
    if (req.session.account) {
        req.query.company_id = req.session.account.company_id;
        if (req.session.account.client === 1) {
            req.query.client_id = req.session.account.client_id;
        }
        warehousesDB.getWarehousesListWithNoCompany(req.query, function(list) {
            res.status('200').send(list);
        });
    } else {
        res.status('500').send(null);
    }
});

router.get('/get_available', function(req, res) {
    if (req.session.account) {
        warehousesDB.getAvailableWarehousesList(req.query.client_id, req.session.account.company_id, function(list) {
            res.status('200').send(list);
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/insert', function(req, res) {
    if (req.session.account.manager === 1) {
        req.body.company_id = req.session.account.company_id;
        warehousesDB.insertWarehouse(req.body, function(result) {
            res.status('200').send({
                result: true
            });
        });
    } else {
        res.status('500').send(null);
    }
});

router.post('/update', function(req, res) {
    if (req.session.account.manager === 1) {
        warehousesDB.updateWarehouse(req.body, function(result) {
            res.status('200').send(result);
        });
    } else {
        res.status('500').send(null);
    }
});

router.delete('/delete', function(req, res) {
    if (req.session.account.manager === 1) {
        warehousesDB.deleteWarehouse(req.body, function(result) {
            res.status('200').send(result);
        });
    } else {
        res.status('500').send(null);
    }
});

module.exports = router;