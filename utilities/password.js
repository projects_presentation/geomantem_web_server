var crypto = require("crypto");
var saltLength = 20;

var generateRandomSalt = function() {
    var randomSalt = crypto.randomBytes(Math.ceil(saltLength / 2))
        .toString('hex')
        .slice(0, saltLength);
    return randomSalt;
};

function sha512(password, salt) {
    /** Hashing algorithm sha512 */
    var hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    return hash.digest('hex');
};

function generateHashedPassword(passwordToHash) {
    var salt, passwordToReturn;
    salt = generateRandomSalt();
    passwordToReturn = sha512(passwordToHash, salt);
    return {
        salt: salt,
        password: passwordToReturn
    };
}

function getHashedPassword(password, salt) {
    return sha512(password, salt);
}

module.exports.generateHashedPassword = generateHashedPassword;
module.exports.getHashedPassword = getHashedPassword;